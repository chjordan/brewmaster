from setuptools import setup
from codecs import open
from os import path
import re

here = path.abspath(path.dirname(__file__))
with open(path.join(here, "brewmaster/__init__.py")) as f:
    contents = f.read()
    version_number = re.search(r"__version__ = \"(\S+)\"", contents).group(1)

with open(path.join(here, "README.rst"), encoding="utf-8") as f:
    long_description = f.read()

setup(name="brewmaster",
      version=version_number,
      description="A radio-astronomy single-dish spectral-line pipeline",
      long_description=long_description,
      url="https://gitlab.com/chjordan/brewmaster",
      author="Christopher H. Jordan",
      author_email="christopherjordan87@gmail.com",
      license="GPLv3",
      keywords="signal processing",
      packages=["brewmaster"],
      package_data={"": ["systemps/*"]},
      scripts=["scripts/brewmaster_miriad2hdf5.py",
               "scripts/brewmaster_quotient.py",
               "scripts/brewmaster_merge.py",
               "scripts/brewmaster_grid.py"],
      install_requires=["numpy>=1.13.0",
                        "scipy",
                        "astropy>=3.0",
                        "h5py",
                        "cygrid",
                        "aipy",
                        "sslf",
                        "tqdm",
                        "matplotlib"],
      )
