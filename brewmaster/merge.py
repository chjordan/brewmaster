import os
import sys
import copy
from io import StringIO
import logging
from collections import defaultdict as dd
import multiprocessing as mp

import numpy as np
from scipy import signal
from scipy.interpolate import InterpolatedUnivariateSpline as spline
from astropy.table import Table
from astropy.coordinates import SkyCoord
import astropy.units as u

import matplotlib.pyplot as plt

from sslf.sslf import _blank_spectrum_part, Spectrum as SLF


# Import tqdm without enforcing it as a dependency
try:
    from tqdm import tqdm
except ImportError:
    def tqdm(*args, **kwargs):
        if args:
            return args[0]
        return kwargs.get('iterable', None)

logger = logging.getLogger(__name__)


class Spectrum:
    def __init__(self, vel, sp, filename, tsys):
        self.vel = vel
        self.spectrum = sp
        self.filename = filename
        self.tsys = tsys
        if len(self.spectrum) < 50:
            self.continuum = True
        else:
            self.continuum = False

    def remove_polynomial(self, poly_order):
        if not self.continuum:
            interp_range = np.arange(len(self.spectrum)).astype(int)
            poly_fit = np.poly1d(np.polyfit(interp_range, self.spectrum, poly_order))
            self.spectrum -= poly_fit(interp_range)

    def create_spline(self, vel_min, vel_max, vel_inc, do_line_find=False,
                      wavelet_scales=None, line_find_snr=6.5, wavelet=signal.ricker):
        """
        Create a spline of the spectrum associated with this object.

        Constructing the spline is simple; perhaps the only thing worth
        mentioning is that any spectral values exactly equal to zero are masked
        as NaNs.

        The most complicated aspect of this function, however, is aligning the
        noise of a spectrum about zero. In the case of a line-free spectrum,
        this routine would simply subtract the spectrum's median value. However,
        some spectra have such bright lines that subtracting their medians make
        the spectrum sit at a non-zero level.

        Strategy: If requested, do a quick line find on the spectrum, mask any
        lines found, and subtract the median of the remaining unmasked spectrum.

        Parameters
        ----------
        vel_min: float
            The minimum of the velocity range.

        vel_max: float
            The maximum of the velocity range.

        vel_inc: float
            The velocity increment, or channel resolution.

        do_line_find: bool, optional
            If true, then perform a line find before subtracting the spectrum's
            line-free median. Otherwise, just subtract the median.

        wavelet_scales: list, optional
            The channel-size scales to use when searching for spectral lines
            before subtracting the residual.

        line_find_snr: float, optional
            The cutoff significance to use when searching for spectral lines
            before subtracting the residual.

        wavelet_scales: callable, optional
            The wavelet to use when performing line finding.
        """

        if wavelet_scales is None:
            wavelet_scales = list(range(4, 20, 2)) + list(range(20, 40, 3)) + list(range(40, 100, 5))

        self.vel_min = vel_min
        self.vel_max = vel_max
        self.vel_inc = vel_inc
        self.vel_full = np.arange(vel_min, vel_max, vel_inc)

        self.spline = spline(self.vel, self.spectrum, ext="zeros")
        spl_spectrum = self.spline(self.vel_full)
        spl_spectrum[spl_spectrum == 0] = np.nan

        if do_line_find:
            # Find any bright lines and mask them before removing the true "median".
            spl_copy = copy.copy(spl_spectrum)
            o = SLF(self.spectrum, self.vel_full)
            o.find_cwt_peaks(scales=wavelet_scales, snr=line_find_snr, wavelet=wavelet)
            for ce in o.channel_edges:
                _blank_spectrum_part(spl_spectrum, ce, value=np.nan)
            self.spline_spectrum = spl_copy - np.nanmedian(spl_spectrum)
        else:
            self.spline_spectrum = spl_spectrum - np.nanmedian(spl_spectrum)


def detect_poorly_behaved_spectra(spectra,
                                  snr=5,
                                  num_chunks=40,
                                  window_length=151,
                                  source_name=None):
    """
    Some spectra have significant ripples (standing waves?); these could confuse
    the line finder and, in the worst cases, "create" a spectral line. Here,
    by looking at all spectra observed in a single spot, we can identify and
    remove these poorly behaved spectra before they are run through the line
    finder.

    Strategy: Produce a trend-following profile of each spectrum with
    `savgol_filter`. Break these spectra into many pieces, and take the standard
    deviation of each piece. Take the variance of the standard deviation blocks,
    and compare each the variance of each spectrum against the median of all of
    the variances of the spectra. Any variances greater than `snr` times the
    median are returned in a new list. The original input `spectra` list is
    altered in place.

    Parameters
    ----------
    spectra : list
        A list of Spectra objects.

    snr : float, optional
        The number specifying how significant the variance is compared to the
        median before being discarded.

    num_chunks : int, optional
        The number of chunks/pieces/blocks to break each spectrum into when
        determining how poorly behaved the spectra are. This should be tweaked
        to match the frequency of any ripples in the spectra.

    window_length : int, optional
        The length of the filter window (i.e. the number of coefficients).
        `window_length` must be a positive odd integer.

    source_name : str, optional
        Specify the source name that the spectra belong to for debugging
        purposes.

    Return
    ------
    out : list
        A list of Spectra objects featuring ripples in their spectra.
    """

    variances = []
    for s in spectra:
        trend = signal.savgol_filter(s.spectrum,
                                     window_length=window_length,
                                     polyorder=1)
        chunks = np.array_split(trend, num_chunks)
        variances.append(np.var([np.std(x) for x in chunks]))

    med = np.median(variances)
    number_flagged = 0
    poorly_behaved = []
    for i, v in enumerate(variances):
        if v > snr * med:
            poorly_behaved.append(spectra.pop(i - number_flagged))
            number_flagged += 1

    if source_name is not None and number_flagged > 0:
        logger.debug("%s: Flagged %s poorly behaved spectra", source_name, number_flagged)

    return poorly_behaved


def _merge_func(params):
    """
    This internal function is designed to be called in parallel, and takes a
    tuple of parameters. `spectra` passed in through `params` should contain
    data collected from the same place on the sky; these data are merged
    together and returned as a single spectrum.
    """

    # Represent each spectrum as a spline so they can be averaged correctly
    # along the velocity axis.
    for sp in params["spectra"]:
        sp.create_spline(params["vel_min"], params["vel_max"], params["vel_inc"],
                         do_line_find=True)

    bandpassed = bandpass_spectra(spectra=params["spectra"],
                                  wavelet_scales=params["wavelet_scales"],
                                  wavelet=params["wavelet"],
                                  blank_additional_channels=params["blank_additional_channels"],
                                  source_name=params["source"],
                                  debugging=params["debugging"])

    # Because cygrid doesn't handle NaN so well, turn them to
    # zeroes, and mask their positions instead.
    nan_indices = np.where(bandpassed == np.nan)[0]
    bandpassed[nan_indices] = 0
    mask = np.ones_like(bandpassed)
    mask[nan_indices] = 0

    ra, dec = params["coordinates"].split('_')
    return (params["source"], ra, dec, bandpassed, mask)


def average_spectra(spectra):
    splines = [x.spline_spectrum for x in spectra]
    weights = [1 / x.tsys**2 for x in spectra]
    vel_full = spectra[0].vel_full
    masked = np.ma.masked_array(splines, np.isnan(splines))

    # Average the spectra correctly in velocity (ignoring a dumb NaN warning).
    with np.errstate(under="ignore"):
        averaged_sp = np.ma.average(masked, axis=0, weights=weights)
    return averaged_sp, vel_full


def flatten_from_averaged(averaged_obj, spectra_objects, window_length=151,
                          bandpass_func=None):
    flattened_spectra = []
    for so in spectra_objects:
        s = SLF(so.spectrum, vel=so.vel)
        if not so.continuum:
            s = SLF(so.spectrum, vel=so.vel)
            s.vel_peaks = averaged_obj.vel_peaks
            s.channel_edges = averaged_obj.channel_edges
            s.peak_snrs = averaged_obj.peak_snrs
            s.vel_peaks2chan_peaks()
            s.subtract_bandpass(window_length=window_length,
                                bandpass_func=bandpass_func)
            so2 = Spectrum(s.vel, s.modified, so.filename, so.tsys)
        else:
            so2 = Spectrum(s.vel, s.original, so.filename, so.tsys)
        so2.create_spline(so.vel_min, so.vel_max, so.vel_inc)

        flattened_spectra.append(so2)
    return flattened_spectra


def flatten_spectra(spectra_container, snr, wavelet_scales, wavelet,
                    window_length=151, blank_additional_channels=(3, 15),
                    bandpass_func=None):
    averaged_container, vel_full = average_spectra(spectra_container)
    acs = averaged_container.size
    # Find any lines.
    slf_a = SLF(averaged_container, vel_full)
    if not spectra_container[0].continuum:
        slf_a.find_cwt_peaks(scales=wavelet_scales, snr=snr, wavelet=wavelet)
        # Pad the sizes of any spectral lines in addition to the
        # refine_line_widths routine.
        slf_a.refine_line_widths(significance_cutoff=1.0)
        for i, (a, b) in enumerate(slf_a.channel_edges):
            dilating_factor = int(blank_additional_channels[0] *
                                  np.sqrt(slf_a.peak_snrs[i]) +
                                  blank_additional_channels[1])
            a -= dilating_factor
            b += dilating_factor
            # Handle edges going over the spectrum length.
            if a < 0:
                a = 0
            if b > acs:
                b = acs
            slf_a.channel_edges[i] = (a, b)
        slf_a.subtract_bandpass(window_length=window_length,
                                bandpass_func=bandpass_func)

    # With the found lines, bandpass the spectra.
    flattened_container = flatten_from_averaged(slf_a, spectra_container, window_length,
                                                bandpass_func=bandpass_func)
    return flattened_container, slf_a


def bandpass_spectra(spectra,
                     wavelet_scales,
                     wavelet,
                     blank_additional_channels=(3, 15),
                     window_lengths=(151, 91, 51, 21),
                     significance_cutoffs=(10, 7, 3, 2.5),
                     flag_poorly_behaved_spectra=True,
                     detect_poorly_behaved_spectra_settings=None,
                     source_name=None,
                     debugging=0):
    """
    While detecting and preserving any spectral lines, convert a list of spectra
    (which hopefully all correspond to the same position on the sky) into a
    flattened, single spectrum.

    Parameters
    ----------
    spectra : list
        A list of Spectra objects.

    wavelet_scales : array_like
        The channel lengths to use when searching for lines.

    detect_poorly_behaved_spectra_settings : dict, optional
        Specify settings for the `detect_poorly_behaved_spectra` function,
        containing: `snr`, `num_chunks` and `window_length`.

    source_name : str, optional
        If supplied, displays the source name in the title of debugging plots.

    debugging : bool, optional
        If true, and this function has identified a spectral line, generate
        plots of the spectra going into a merge, and the line-finding procedure.
    """

    if isinstance(window_lengths, int):
        window_lengths = [window_lengths] * len(significance_cutoffs)
    elif len(window_lengths) != len(significance_cutoffs):
        raise ValueError("window_lengths needs to be the same length as significance_cutoffs.")

    if len(spectra[0].spectrum) < 50:
        continuum = True
    else:
        continuum = False

    if detect_poorly_behaved_spectra_settings is None:
        detect_poorly_behaved_spectra_settings = {"snr": 5, "num_chunks": 40, "window_length": 151}

    if not continuum and flag_poorly_behaved_spectra:
        poor_spectra = detect_poorly_behaved_spectra(spectra,
                                                     source_name=source_name,
                                                     **detect_poorly_behaved_spectra_settings)
    else:
        poor_spectra = []

    flattened_spectra_container = spectra
    sslf_objs = []
    for i, significance_cutoff in enumerate(significance_cutoffs):
        if i != len(significance_cutoffs) - 1:
            bpf = None
        else:
            bpf = lambda x: signal.medfilt(x, window_lengths[i])
        flattened_spectra_container, sslf_obj = \
            flatten_spectra(flattened_spectra_container, significance_cutoff,
                            wavelet_scales, wavelet, window_lengths[i],
                            blank_additional_channels, bandpass_func=bpf)
        sslf_objs.append(sslf_obj)

    # Put the poorly-behaved spectra back in with the others.
    # Use sslf to remove the bandpass after telling it where the lines
    # are, then re-do a line find using all spectra.
    if poor_spectra:
        for p in poor_spectra:
            b = SLF(p.spectrum, p.vel_full)
            b.vel_peaks = sslf_obj.vel_peaks
            b.channel_edges = sslf_obj.channel_edges
            b.peak_snrs = sslf_obj.peak_snrs
            b.vel_peaks2chan_peaks()
            b.subtract_bandpass()
            o = Spectrum(p.vel,
                         b.modified,
                         p.filename,
                         p.tsys)
            o.create_spline(flattened_spectra_container[0].vel_min,
                            flattened_spectra_container[0].vel_max,
                            flattened_spectra_container[0].vel_inc)
            flattened_spectra_container.append(o)
        flattened_spectra_container, sslf_obj = \
            flatten_spectra(flattened_spectra_container, 6.5, wavelet_scales,
                            wavelet, window_lengths[0],
                            blank_additional_channels)
        sslf_objs.append(sslf_obj)

    # Averaged is the final spectrum to be returned from this function.
    averaged, vel_full = average_spectra(flattened_spectra_container)

    def plot_averaged(ob, axis):
        axis.plot(ob.vel, ob.original, c='b', zorder=0)
        axis.plot(ob.vel, ob.filtered, c='g', zorder=0)
        axis.plot(ob.vel, ob.bandpass, c='r', zorder=0)
        axis.scatter(ob.vel_peaks, ob.original[ob.channel_peaks],
                     c='r', marker='x', s=200, lw=2, zorder=1)
        axis.grid()

    if (debugging == 1 and sslf_objs[-1].vel_peaks) or debugging > 1:
        logger.debug("Peaks and their widths: %s, %s" % (sslf_obj.vel_peaks, sslf_obj.channel_edges))

        sslf_obj = sslf_objs[-1]
        if not continuum:
            sslf_obj.subtract_bandpass()

        fig, ax = plt.subplots(len(sslf_objs) + 2, 1, sharex=True)
        fig.subplots_adjust(left=0.05,
                            bottom=0.05,
                            right=0.99,
                            top=0.95,
                            wspace=0.2,
                            hspace=0.05)
        splines = [x.spline_spectrum for x in spectra]
        tsys_values = [x.tsys for x in spectra]
        vel_full = spectra[0].vel_full
        for i, spl in enumerate(splines):
            ax[0].plot(vel_full, spl, alpha=0.75, label=tsys_values[i])
        ax[-1].plot(vel_full, averaged, label=tsys_values[i])
        ax[-1].grid()

        for i, obj in enumerate(sslf_objs):
            plot_averaged(obj, ax[i + 1])

        ax[-1].set_xlim(sslf_obj.vel[0], sslf_obj.vel[-1])
        for i in range(1, len(sslf_objs) + 2):
            ax[i].set_ylim(1.5 * np.min(sslf_obj.original),
                           1.5 * np.max(sslf_obj.original))

        # ax[0].legend()
        if source_name is not None:
            ax[0].set_title(source_name)
        plt.show()
        plt.close()

    return averaged


class Merge:
    def __init__(self, files, debugging=0, xx_only=False, yy_only=False, path="quotiented"):
        # Sanity checking.
        if xx_only and yy_only:
            raise ValueError("xx_only and yy_only cannot both be enabled.")
        if xx_only:
            self.pol = "xx"
        elif yy_only:
            self.pol = "yy"
        else:
            self.pol = "xxyy"

        # Input handling.
        # We expect a list of files here, so if a single file was passed
        # instead, we need to put it in a list.
        if isinstance(files, (str, Table)):
            files = [files]
        if isinstance(files, list):
            self.tables = {}
            # The elements of the files list could be different types.
            for i, f in enumerate(files):
                if isinstance(f, str):
                    # Check that the read permissions on the file are OK.
                    if not os.access(f, os.R_OK):
                        raise IOError("No read permissions on file {0}!".format(f))
                    try:
                        # Check that `f` isn't already in the dict
                        # (prevents duplicated data).
                        if f in self.tables:
                            raise ValueError("{0} appears to be duplicate data!".format(f))
                        self.tables[f] = Table.read(f, path=path)
                    except IOError:
                        logger.warning("%s does not have a %s path.", f, path)
                elif isinstance(f, Table):
                    dummy_file_name = "file_{0}".format(i)
                    if dummy_file_name in self.tables:
                        raise ValueError("{0} appears to be duplicate data!".format(dummy_file_name))
                    self.tables[dummy_file_name] = f
                else:
                    raise ValueError("Input unrecognised!")
        else:
            raise ValueError("Input unrecognised!")

        self.files = files
        self.debugging = debugging

        # Try to determine what telescope this data comes from.
        try:
            first = list(self.tables.keys())[0]
            self.telescope = self.tables[first].meta["telescope"]
            self.rest_freq = self.tables[first].meta["rest_freq"]
            self.extended = self.tables[first].meta["extended"]
            self.continuum = self.tables[first].meta["continuum"]
        # If there is no metadata available, assume this data has been generated
        # for testing purposes.
        except KeyError:
            self.telescope = None
            self.rest_freq = None

    def find_common_sources(self, filter_all_sources_except=None):
        """
        Get the sources from each table. Group sources together that
        have the same name and coordinates.
        """

        # source -> coords -> file -> indices
        self.sources = dd(lambda: dd(lambda: dd(lambda: {})))
        # Smallest and largest velocities everywhere.
        self.vel_min = ""
        self.vel_max = ""

        for f, t in tqdm(self.tables.items(),
                         desc="Unpacking tables",
                         disable=not logger.isEnabledFor(logging.INFO)):
            unique_sources = sorted(set(t["source"]))

            vel_min_f = np.min(t["vel_end"])
            vel_max_f = np.max(t["vel_start"])
            if self.vel_min == "" or vel_min_f < self.vel_min:
                self.vel_min = vel_min_f
            if self.vel_max == "" or vel_max_f > self.vel_max:
                self.vel_max = vel_max_f

            for un in unique_sources:
                if filter_all_sources_except is not None and \
                   un != filter_all_sources_except:
                    continue
                indices = np.where(t["source"] == un)[0]
                ra, dec = t["ra"][indices], t["dec"][indices]
                vel_min = t["vel_end"][indices]
                vel_max = t["vel_start"][indices]
                vel_inc = t["vel_inc"][indices]

                for j, i in enumerate(indices):
                    coord = "%.5f_%.5f" % (ra[j], dec[j])
                    self.sources[un][coord][f][i] = {"vel_end": vel_min[j],
                                                     "vel_start": vel_max[j],
                                                     "vel_inc": vel_inc[j]}

        # Assumes all spectra have the same length.
        self.vel_inc = vel_inc[j]
        if self.vel_max < self.vel_min:
            self.vel_min, self.vel_max = self.vel_max, self.vel_min

    def merge_spectra(self,
                      scales,
                      blank_additional_channels=(3, 15),
                      poly_order=1,
                      wavelet=signal.ricker,
                      processes=mp.cpu_count()):
        # to_be_merged is a list of tuples that will be eventually be used to
        # process data in parallel.
        to_be_merged = []
        spectra = []

        # source -> coords -> file -> indices
        for s in self.sources:
            for c in self.sources[s]:
                logger.debug("%s, %s" % (s, c))
                for f in self.sources[s][c]:
                    for i in self.sources[s][c][f]:
                        vs = self.sources[s][c][f][i]["vel_start"]
                        ve = self.sources[s][c][f][i]["vel_end"]
                        if vs < ve:
                            vel_min = vs
                            vel_max = ve
                        else:
                            vel_min = ve
                            vel_max = vs
                        xx = self.tables[f]["xx"][i]
                        yy = self.tables[f]["yy"][i]

                        # Double the Tsys of antenna 6.
                        if "antenna" in self.tables[f].meta and \
                           self.tables[f].meta["antenna"] == 6:
                            xx_tsys = self.tables[f]["xx_tsys"][i] * 2
                            yy_tsys = self.tables[f]["yy_tsys"][i] * 2
                        else:
                            xx_tsys = self.tables[f]["xx_tsys"][i]
                            yy_tsys = self.tables[f]["yy_tsys"][i]
                        vel = np.linspace(vel_min, vel_max, len(xx))

                        if ve < vs:
                            xx = xx[::-1]
                            yy = yy[::-1]

                        # Assumes data is unpolarised - stacks XX and YY.
                        if not np.all(np.isnan(xx)) and "xx" in self.pol:
                            obj = Spectrum(vel, xx, f, xx_tsys)
                            obj.remove_polynomial(poly_order=poly_order)
                            spectra.append(obj)
                        if not np.all(np.isnan(yy)) and "yy" in self.pol:
                            obj = Spectrum(vel, yy, f, yy_tsys)
                            obj.remove_polynomial(poly_order=poly_order)
                            spectra.append(obj)

                # Add the data to be merged.
                if spectra:
                    to_be_merged.append({"source": s,
                                         "coordinates": c,
                                         "spectra": spectra,
                                         "vel_min": self.vel_min,
                                         "vel_max": self.vel_max,
                                         "vel_inc": self.vel_inc,
                                         "wavelet_scales": scales,
                                         "blank_additional_channels": blank_additional_channels,
                                         "wavelet": wavelet,
                                         "debugging": self.debugging})

                    # Reset spectra.
                    spectra = []

        # Process to_be_merged
        # For annoying reasons, we can't use tqdm's disable here.
        # https://github.com/tqdm/tqdm/issues/324
        if logger.isEnabledFor(logging.INFO):
            tqdm_file = sys.stdout
        else:
            tqdm_file = StringIO()
        pool = mp.Pool(processes=processes)
        merged = list(tqdm(pool.imap(_merge_func,
                                     to_be_merged,
                                     chunksize=20),
                           total=len(to_be_merged),
                           desc="Line finding",
                           file=tqdm_file))
        pool.close()

        # Unpack the results.
        sources = []
        ras = []
        decs = []
        spectra = []
        masks = []

        for m in merged:
            source, ra, dec, spectrum, mask = m
            sources.append(source)
            ras.append(ra)
            decs.append(dec)
            spectra.append(spectrum)
            masks.append(mask)

        output_spectra = {"spectrum": spectra,
                          "mask": masks}

        # Now that we have all the relevant RA/Dec. coordinates,
        # calculate the corresponding Galactic coordinates.
        skycoord = SkyCoord(ra=ras, dec=decs,
                            unit=(u.deg, u.deg), frame="icrs")
        output_spectra["glon"] = skycoord.galactic.l.deg
        output_spectra["glat"] = skycoord.galactic.b.deg

        # Prevent encoding errors with h5py
        output_spectra["ra"] = np.array(ras, dtype='S')
        output_spectra["dec"] = np.array(decs, dtype='S')
        output_spectra["source"] = np.array(sources, dtype='S')
        self.output_spectra = output_spectra

        self.meta = {"vel_min": self.vel_min,
                     "vel_max": self.vel_max,
                     "vel_inc": self.vel_inc,
                     "rest_freq": self.rest_freq,
                     "telescope": self.telescope,
                     "extended": self.extended,
                     "continuum": self.continuum}
        if any([isinstance(x, str) for x in self.files]):
            self.meta["derived_from"] = ','.join([x for x in self.files if isinstance(x, str)])
        self.table = Table(self.output_spectra,
                           names=("source",
                                  "ra",
                                  "dec",
                                  "glon",
                                  "glat",
                                  "spectrum",
                                  "mask"),
                           meta=self.meta)

    def write(self, output_filename, path="data"):
        self.output_filename = output_filename

        logger.info("Writing %s in the \"%s\" path ... " % (self.output_filename, path))
        self.table.write(self.output_filename, path=path,
                         compression=True, overwrite=True, append=True)
        logger.info("done.")
