import os
import logging
from datetime import datetime as dt

import numpy as np
from scipy import signal
from astropy.table import Table
from astropy.coordinates import SkyCoord
import astropy.units as u

import matplotlib.pyplot as plt

from sslf.sslf import Spectrum as SLF

from brewmaster.gains import Obs, jyperk
from brewmaster.utils import interpolate_over_line, detect_and_flag_spikes


# Import tqdm without enforcing it as a dependency
try:
    from tqdm import tqdm
except ImportError:
    def tqdm(*args, **kwargs):
        if args:
            return args[0]
        return kwargs.get('iterable', None)

logger = logging.getLogger(__name__)
np.seterr(all="raise")


def miriad2datetime(dates):
    def convert(d):
        return dt.strptime(d, "%y%b%d:%H:%M:%S.%f")

    if isinstance(dates, np.bytes_):
        return convert(dates.decode("UTF-8"))
    elif isinstance(dates, str):
        return convert(dates)
    elif isinstance(dates, (list, np.ndarray)):
        return [convert(d) for d in dates]


class Calibrate:
    def __init__(self, inp, extended=True, rename_references=True, reference="ref",
                 edge_ignore=False, birdies=False, path="raw"):
        # Input handling.
        # The input can be a string for a filepath, or an astropy table object.
        if isinstance(inp, str):
            self.filename = inp
            self.basename = os.path.basename(inp)
            self.table = Table.read(inp, path=path)
        elif isinstance(inp, Table):
            self.filename = None
            self.table = inp
        else:
            raise ValueError("Input unrecognised!")

        # Sanity check: are there any data in the table?
        if len(self.table) == 0:
            raise ValueError("No rows in the table!")

        # Try to determine what telescope this data comes from.
        try:
            self.telescope = self.table.meta["telescope"].decode("UTF-8")
        # If we get an error about decode, this could be a python2 vs 3 thing.
        # Just try without decode.
        except AttributeError:
            self.telescope = self.table.meta["telescope"]
        # If there is no metadata available, assume this data has been generated
        # for testing purposes.
        except KeyError:
            self.telescope = None
        logger.debug("Telescope is %s" % self.telescope)

        # Determine the number of channels. This assumes that all spectra
        # in the table have the same number of channels.
        self.num_channels = self.table.meta["num_channels"]

        # Apply some telescope-specific rules.
        if self.telescope == "ATCA":
            logger.info("ATCA data detected.")
            if self.num_channels > 1000:
                self.birdies = range(128, self.num_channels, 128)
                # 5% of the bandpass, rounded to the smallest multiple of 50.
                self.edge_ignore = self.num_channels // 20 // 50 * 50
                self.continuum = False
            elif self.num_channels == 33:
                # 32 channel continuum bands.
                self.birdies = range(8, self.num_channels, 8)
                self.edge_ignore = 1
                self.continuum = True

            # Check if we have velocity columns. If not, we derive them.
            columns = self.table.columns
            if not ("vel_inc" in columns
                    and "vel_start" in columns
                    and "vel_end" in columns):
                self._derive_velocities()
        else:
            logger.info("Telescope either not recognised or not specified.")
            self.birdies = []
            self.edge_ignore = 0
            self.continuum = False
            # Check if we have velocity columns. If not, we throw an exception.
            columns = self.table.columns
            if not ("vel_inc" in columns
                    and "vel_start" in columns
                    and "vel_end" in columns):
                raise ValueError("Velocity columns (vel_inc, vel_start and vel_end) are missing!")
        logger.debug("continuum mode: %s" % self.continuum)

        # The "extended" refers to whether the emission contained in these data
        # is assumed to be extended or point-source-like. This parameter allows
        # the data to be multiplied by the Jy-per-K factor in this code.
        self.extended = extended
        logger.debug("extended emission: %s" % self.extended)

        # If birdies and/or edge_ignore were explicitly specified, use them.
        if birdies is None:
            self.birdies = []
        elif birdies is not False:
            self.birdies = birdies

        if edge_ignore is None:
            self.edge_ignore = 0
        elif edge_ignore is not False:
            self.edge_ignore = edge_ignore

        logger.debug("birdies is %s" % self.birdies)
        logger.debug("edge_ignore is %s" % self.edge_ignore)

        # Paranoia check for edge_ignore being too big.
        if self.edge_ignore >= self.num_channels / 2:
            raise ValueError("edge_ignore ({0}) exceeds 50% of the "
                             "data ({1} total channels).".format(self.edge_ignore, self.num_channels))

        # If we are to rename the reference source names,
        # we assume that they already contain "ref".
        if rename_references:
            for i, s in enumerate(self.table["source"]):
                if "ref" in s:
                    self.table[i]["source"] = reference

        # Check that the specified reference is actually in the table.
        if not np.any(self.table["source"] == reference):
            raise ValueError("Reference (%s) is not in the table (%s)." %
                             (reference, self.filename))
        self.reference = reference
        logger.debug("reference is %s" % self.reference)

        # Save some additional variables into the table metadata.
        meta = self.table.meta
        meta["jyperk"] = jyperk(meta["rest_freq"] * 1000, self.telescope)
        meta["extended"] = self.extended
        meta["continuum"] = self.continuum
        self.table.meta = meta

    def _derive_velocities(self):
        # Same code as MIRIAD's uvlist.for, but edited with Maxim Voronkov's "dopdet".
        c = 299792458
        t = self.table
        beta = (t["veldop"] - t["vsource"]) / c * 1000
        dopdet = np.sqrt(1 - beta**2) / (1 - beta)

        t["veldop"] -= t["vsource"]
        t["vel_inc"] = 0.001 * c * t.meta["sdf"] / t.meta["rest_freq"] * dopdet
        t["vel_start"] = 0.001 * c * (1 - t.meta["sfreq"] * dopdet / t.meta["rest_freq"])
        t["vel_end"] = t["vel_start"] - t["vel_inc"] * (self.num_channels - 1)

        t.remove_columns(["veldop", "vsource"])

    def prune(self, prune_radius=5):
        skycoord = SkyCoord(ra=self.table["ra"], dec=self.table["dec"],
                            unit=(u.deg, u.deg), frame="fk5")
        med_l = np.median(skycoord.galactic.l.deg)
        logger.debug("Median Galactic longitude is %s" % med_l)

        # If the prune_radius is 0 or None, we effectively disable pruning.
        if prune_radius == 0 or prune_radius is None:
            prune_radius = np.inf

        bad_indices = []
        for i, s in enumerate(skycoord.galactic.l.deg):
            if abs(s - med_l) > prune_radius:
                bad_indices.append(i)
                logger.debug("Pruning source %s" % self.table[i]["source"])
        self.table.remove_rows(bad_indices)

    def flag_birdies(self, median_width=3):
        """
        Correct any known correlator artefacts ("birdies").

        Parameters
        ----------
        median_width : int, optional
            Use this many channels on either side of a birdie channel to
            determine the interpolated value (this feeds directly into the
            `interpolate_over_line` function).
        """

        for i, _ in enumerate(tqdm(self.table,
                                   desc="Flagging birdies",
                                   disable=not logger.isEnabledFor(logging.INFO))):
            for p in ["xx", "yy"]:
                # Correct birdie channels.
                for x in self.birdies:
                    spectrum = self.table[p][i]
                    interpolate_over_line(spectrum, x, edge_channels=(0, len(spectrum)),
                                          median_width=3, padding=0, poly_order=1)

    def flag_rfi(self, median_width=3, smooth_window=3):
        """
        Correct any telescope-specific oddities.

        Parameters
        ----------
        median_width : int, optional
            Use this many channels on either side of a RFI channel to
            determine the interpolated value (this feeds directly into the
            `interpolate_over_line` function).

        smooth_window : int, optional
            When smoothing spectra, use this many channels for the smoothing
            window.
        """

        if self.telescope == "ATCA":
            bad_indices = []
            vel_end_med = np.median(self.table["vel_end"])
            vel_start_med = np.median(self.table["vel_start"])
            logger.debug("Median start velocity is %s" % vel_start_med)
            logger.debug("Median end velocity is %s" % vel_end_med)

            for i, _ in enumerate(tqdm(self.table,
                                       desc="Flagging RFI",
                                       disable=not logger.isEnabledFor(logging.INFO))):
                for p in ["xx", "yy"]:
                    # Some scans can have stupidly offset velocities.
                    if abs(self.table["vel_end"][i] - vel_end_med) > 100 or \
                       abs(self.table["vel_start"][i] - vel_start_med) > 100:
                        successful_flag = False
                        logger.debug("Scan %s flagged due to abnormal velocity range" %
                                     self.table["source"][i])
                        continue

                    # Not enough is known about RFI in continuum data; for now, do not
                    # try to do any further flagging.
                    if self.continuum:
                        continue

                    # More advanced routines.
                    successful_flag = self._rfi_flag(index=i,
                                                     pol=p,
                                                     median_width=median_width,
                                                     smooth_window=smooth_window)

                    if not successful_flag:
                        # dumped_filename = "%s_%s_%s_%s" % (self.basename,
                        #                                    self.table["source"][i],
                        #                                    p,
                        #                                    self.table["date"][i])
                        # bm.dump_spectrum_image(self.table[p][i],
                        #                        dumped_filename,
                        #                        directory="FLAGGED_SPECTRA")
                        # spectrum.fill(np.nan)
                        bad_indices.append(i)

            self.table.remove_rows(bad_indices)

        else:
            logger.warning("No telescope associated with the data; not performing RFI flagging.")

    def _rfi_flag(self, index, pol, median_width=3, smooth_window=3):
        spectrum = self.table[pol][index]
        scan_name = self.table["source"][index]

        # Find and attempt to correct single channel spikes.
        detect_and_flag_spikes(spectrum,
                               threshold=1000,
                               scan_name=scan_name,
                               median_width=median_width,
                               smooth_window=smooth_window,
                               edge_ignore=self.edge_ignore)

        # Normal ATCA spectra have bandpasses typically with values of order 1e4.
        # If a spectrum has very small values, it may indicate a poorly-behaved
        # correlator.
        if np.any(spectrum < 1000):
            logger.debug("Scan %s flagged - part of its spectrum < 1000" % scan_name)
            return False

        # Identify CABB fuzzies - sections of the spectrum with huge variance.
        filtered = signal.savgol_filter(spectrum, window_length=51, polyorder=1)
        if self.edge_ignore == 0:
            reduced = spectrum - filtered
        else:
            reduced = spectrum[self.edge_ignore:-self.edge_ignore] - \
                filtered[self.edge_ignore:-self.edge_ignore]
        splitted = np.array_split(reduced, 50)
        stds = [np.std(x) for x in splitted]
        minmax = [np.max(x) - np.min(x) for x in splitted]
        if max(stds) > 1000 and max(minmax) > 1e4:
            logger.debug("Scan %s flagged - large variation in amplitude" % scan_name)
            return False

        return True

    def flag_weak_rfi(self, median_width=3, smooth_window=3):
        """
        Correct channel spikes that are comparably weak to those found in
        `flag_rfi`, but appear in similar channels over many spectra of an
        observation.

        Parameters
        ----------
        median_width : int, optional
            Use this many channels on either side of a RFI channel to
            determine the interpolated value (this feeds directly into the
            `interpolate_over_line` function).

        smooth_window : int, optional
            When smoothing spectra, use this many channels for the smoothing
            window.
        """

        # Average all spectra for each polarisation, to find weaker
        # correlator artefacts.
        for p in ["xx", "yy"]:
            average = np.mean(self.table[p], axis=0)
            s = SLF(average)
            s.find_cwt_peaks(scales=np.arange(1, 3), snr=8)
            bad_channels = s.channel_peaks

            # Find clusters of bad channels to flag them appropriately.
            # to_be_flagged is a dict with keys representing the centres of
            # bad channel clusters, and values of the cluster widths.
            to_be_flagged = {}
            for b in sorted(np.unique(bad_channels)):
                cluster = [b]
                shift = 1
                while b - shift in bad_channels:
                    cluster.append(b - shift)
                    shift += 1
                shift = 1
                while b + shift in bad_channels:
                    cluster.append(b + shift)
                    shift += 1

                centre = np.median(cluster).astype(int)
                size = len(cluster) // 2 + 1
                to_be_flagged[centre] = size

            for spectrum in tqdm(self.table[p],
                                 desc="Flagging correlator artefacts in {0}-polarisation data".format(p),
                                 disable=not logger.isEnabledFor(logging.INFO)):
                for centre, pad in to_be_flagged.items():
                    interpolate_over_line(spectrum,
                                          channel_range=centre,
                                          median_width=3,
                                          padding=pad,
                                          edge_channels=(0, len(spectrum)))

    def calibrate(self):
        """
        This method eliminates the elevation dependence on the bandpass gain,
        and uses the ATCA sensitivity calculator to determine what the system
        temperature should be, based on the weather parameters.
        """

        # Dividing through by measured system temperatures and Jy-per-K factors
        # gives a bandpass that is independent of elevation.
        self.table["xx"] = (self.table["xx"].T / self.table["jyperk"] / self.table["xx_tsys"]).T
        self.table["yy"] = (self.table["yy"].T / self.table["jyperk"] / self.table["yy_tsys"]).T

        # Determine what the Tsys *should* be - using the observing frequency
        # and weather parameters.
        o = Obs(self.table.meta["rest_freq"] * 1000,  # MHz
                self.table["temp"] + 273.15,          # K
                self.table["pressure"] * 100,         # Pa
                self.table["humidity"] / 100,         # Absolute fraction
                self.table["elev"],                   # Degrees
                self.table["dec"],                    # Degrees
                self.telescope)
        o.determine_tsys()
        self.table["xx_tsys"] = o.tsys
        self.table["yy_tsys"] = o.tsys

    def _average(self, ref_dict, smoothing, window):
        """
        Helper function to average reference data together.
        """
        xx, yy = ref_dict["xx"], ref_dict["yy"]
        xx_tsys, yy_tsys = ref_dict["xx_tsys"], ref_dict["yy_tsys"]
        elev = ref_dict["elev"]
        if xx:
            xx = np.mean(xx, axis=0)
            if not self.continuum and smoothing:
                xx = np.convolve(xx, window, mode="same")
        if yy:
            yy = np.mean(yy, axis=0)
            if not self.continuum and smoothing:
                yy = np.convolve(yy, window, mode="same")
        if xx_tsys:
            xx_tsys = np.mean(xx_tsys)
        if yy_tsys:
            yy_tsys = np.mean(yy_tsys)
        elev = np.mean(elev)
        return xx, yy, xx_tsys, yy_tsys, elev

    def _assign_averaged_data(self, ref_dict, primary_index, smoothing, window):
        """
        Helper function to alter self.table with averaged reference data.
        """
        logger.log(1, "Averaging reference spectra")
        xx, yy, xx_tsys, yy_tsys, elev = self._average(ref_dict, smoothing, window)

        # Check that there are actually spectra available.
        if ref_dict["xx"]:
            self.table["xx"][primary_index] = xx
        if ref_dict["yy"]:
            self.table["yy"][primary_index] = yy
        if ref_dict["xx_tsys"]:
            self.table["xx_tsys"][primary_index] = xx_tsys
        if ref_dict["yy_tsys"]:
            self.table["yy_tsys"][primary_index] = yy_tsys
        if ref_dict["elev"]:
            self.table["elev"][primary_index] = elev

    def average_references(self, smoothing=True, smooth_window=None, remove_first_ref=False):
        # Handle the smooth_window parameter. If not specified, calculate a default.
        if self.telescope == "ATCA":
            if not self.continuum:
                # 1% of the bandpass, and ensure it's odd.
                self.smooth_window = self.num_channels // 100
                if self.smooth_window % 2 == 0:
                    self.smooth_window += 1
            else:
                self.smooth_window = 1
        else:
            self.smooth_window = 1

        if smooth_window is not None:
            # Ensure the smooth window is odd.
            if smooth_window % 2 == 0:
                logger.debug("smooth_window was %s, but it has been incremented to make it odd"
                             % smooth_window)
                smooth_window += 1
            self.smooth_window = smooth_window
        logger.debug("smoothing reference spectra: %s" % smoothing)
        if smoothing:
            logger.debug("smooth_window is %s" % self.smooth_window)

        window = np.hanning(self.smooth_window)
        window /= np.sum(window)

        if remove_first_ref:
            logger.debug("Removing the first reference from all reference scan blocks")
            # If the first reference scan in a block of reference scans is
            # always bad, flag it.
            bad_indices = []
            in_ref_block = False
            for i, _ in enumerate(self.table):
                if self.table["source"][i] == self.reference:
                    if not in_ref_block:
                        in_ref_block = True
                        bad_indices.append(i)
                        logger.log(1, "Removing reference scan at time %s" % self.table["date"][i])
                        continue
                elif in_ref_block:
                    in_ref_block = False
            self.table.remove_rows(bad_indices)

        if self.continuum and smoothing:
            logger.debug("Smoothing requested but operating in continuum mode; refusing to smooth.")

        ref_spectra = {"xx": [], "yy": [], "xx_tsys": [], "yy_tsys": [], "elev": []}
        merged = []
        in_ref_block = False

        for i, _ in enumerate(tqdm(self.table,
                                   desc="Averaging reference scans",
                                   disable=not logger.isEnabledFor(logging.INFO))):
            if self.table["source"][i] == self.reference:
                if not in_ref_block:
                    in_ref_block = True
                    primary = i
                else:
                    merged.append(i)
                logger.log(1, "Adding reference scan at time %s for averaging" % self.table["date"][i])

                if not np.any(np.isnan(self.table["xx"][i])):
                    ref_spectra["xx"].append(self.table["xx"][i])
                if not np.any(np.isnan(self.table["yy"][i])):
                    ref_spectra["yy"].append(self.table["yy"][i])
                if not np.any(np.isnan(self.table["xx"][i])):
                    ref_spectra["xx_tsys"].append(self.table["xx_tsys"][i])
                if not np.any(np.isnan(self.table["yy"][i])):
                    ref_spectra["yy_tsys"].append(self.table["yy_tsys"][i])
                if (not np.any(np.isnan(self.table["xx"][i]))) or (not np.any(np.isnan(self.table["yy"][i]))):
                    ref_spectra["elev"].append(self.table["elev"][i])

            # The first non-reference scan following a block of reference scans.
            elif in_ref_block:
                self._assign_averaged_data(ref_spectra, primary, smoothing, window)
                in_ref_block = False
                ref_spectra = {"xx": [], "yy": [], "xx_tsys": [], "yy_tsys": [], "elev": []}

        # Deal with any reference data at the very end of the table.
        if in_ref_block:
            self._assign_averaged_data(ref_spectra, primary, smoothing, window)

        self.table.remove_rows(merged)

    def quotient(self, debugging=False):
        """
        Iterate over the source scans, selecting the best reference scan to use
        for a quotient operation. Before quotienting, if edges are to be
        ignored, trim them off, and adjust the velocity range accordingly.
        """

        self.debugging = debugging

        previous_time = 0
        previous_ref = -1
        ref_indices = []
        data_indices = []

        for i, _ in enumerate(tqdm(self.table,
                                   desc="Quotienting",
                                   disable=not logger.isEnabledFor(logging.INFO))):
            name = self.table["source"][i]
            time = miriad2datetime(self.table["date"][i])
            # Initialise the time so that the following conditional works.
            if previous_time == 0:
                previous_time = time

            # If the difference between scans is too big...
            if (time - previous_time).seconds > 1800:
                # ... quotient the stored data now before proceeding.
                # This assumes that there are always data accompanied by references.
                self._quotient_scans(data_indices, previous_ref)

                # Reset the data scans, and go back to scan i.
                data_indices = []

            # Save this time to compare to the next scan.
            previous_time = time

            if name == self.reference:
                ref_indices.append(i)
                if previous_ref == -1:
                    previous_ref = i
                else:
                    current_ref = i

                    # Use the reference that's closest to the data.
                    ln = len(data_indices)
                    self._quotient_scans(data_indices[:ln // 2], previous_ref)
                    self._quotient_scans(data_indices[ln // 2:], current_ref)
                    data_indices = []
                    previous_ref = current_ref

            else:
                data_indices.append(i)

        # Baseline the data following the last reference.
        try:
            current_ref
        except NameError:
            current_ref = previous_ref

        self._quotient_scans(data_indices, current_ref)

        # Remove references from the table.
        self.table.remove_rows(ref_indices)

        # If the emission is not extended (i.e. a point source), multiply by the
        # Jy-per-K factor.
        if not self.extended:
            self.table["xx"] *= self.table.meta["jyperk"]
            self.table["yy"] *= self.table.meta["jyperk"]

        # Fix the edges of the spectra, according to edge_ignore.
        if self.edge_ignore != 0:
            self.table["xx"] = self.table["xx"][:, self.edge_ignore:-self.edge_ignore]
            self.table["yy"] = self.table["yy"][:, self.edge_ignore:-self.edge_ignore]

            # Fix the velocity ranges, according to edge_ignore.
            for i, _ in enumerate(self.table):
                vel_min = self.table["vel_start"][i]
                vel_max = self.table["vel_end"][i]
                vel_inc = self.table["vel_inc"][i]
                if vel_max < vel_min:
                    vel_inc *= -1

                vel_full = np.arange(vel_min, vel_max, vel_inc)
                self.table["vel_start"][i] = vel_full[self.edge_ignore]
                self.table["vel_end"][i] = vel_full[-self.edge_ignore]

    def _quotient_scans(self, data_indices, ref_index):
        for d in data_indices:
            for p in ["xx", "yy"]:
                source = self.table[p][d]
                # source_tsys = self.table[p + "_tsys"][d]
                ref = self.table[p][ref_index]
                ref_tsys = self.table[p + "_tsys"][ref_index]

                if self.debugging:
                    if self.edge_ignore != 0:
                        plt.plot(source[self.edge_ignore:-self.edge_ignore],
                                 label="%s %s (%s)" % (self.table["source"][d], p, d))
                        plt.plot(ref[self.edge_ignore:-self.edge_ignore],
                                 label="Reference %s (%s)" % (p, ref_index))
                    else:
                        plt.plot(source, label="%s %s (%s)" % (self.table["source"][d], p, d))
                        plt.plot(ref, label="Reference %s (%s)" % (p, ref_index))

                self.table[p][d] = (source - ref) / ref * ref_tsys
                self.table[p + "_tsys"][d] = ref_tsys

                # # livedata
                # self.table[p][d] = (source*1.25 - ref) / ref * 71
                # self.table[p+"_tsys"][d] = 71

                if self.debugging:
                    if self.edge_ignore != 0:
                        plt.plot(self.table[p][d][self.edge_ignore:-self.edge_ignore],
                                 label="Quotient " + p)
                    else:
                        plt.plot(self.table[p][d], label="Quotient " + p)

                logger.log(1, "Scan %s quotiented with ref %s in pol %s. Tsys = %s" %
                           (d, ref_index, p, ref_tsys))

            if self.debugging:
                plt.title(self.table["source"][d])
                plt.legend()
                plt.show()
                plt.close()

    def write(self, output_filename=None, path="quotiented"):
        if output_filename is None and self.filename is not None:
            self.output_filename = self.filename
        elif output_filename is None and self.filename is None:
            raise ValueError("No output filename specified!")

        logger.info("Writing to the \"%s\" path..." % path)
        self.table.write(self.output_filename, path=path,
                         compression=True, overwrite=True, append=True)
        logger.info("done.")
