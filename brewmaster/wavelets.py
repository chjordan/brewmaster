import numpy as np
from scipy import signal


def wide_and_flat_topped(points, a):
    vec = np.arange(0, points) - (points - 1.0) / 2
    func = -1 / a**2 * vec**2 + 1
    func[func < 0] = 0
    return func


def tukey(points, a):
    return signal.tukey(points, a / points)


def lorentzian(points, a):
    vec = np.arange(0, points) - (points - 1.0) / 2
    a2 = a**2
    func = a2 / (a2 + 4 * vec**2)
    return func
