import os
import sys
import copy
import shutil
import logging

import numpy as np
from astropy.io import fits as pyfits
from astropy.wcs import WCS


logger = logging.getLogger(__name__)


def fits_cube_hdr2fits_map_hdr(hdr, bunit=None):
    output_header = {}
    for k in ["NAXIS1", "CRPIX1", "CTYPE1", "CUNIT1", "CDELT1", "CRVAL1",
              "NAXIS2", "CRPIX2", "CTYPE2", "CUNIT2", "CDELT2", "CRVAL2",
              "SPECSYS"]:
        output_header[k] = hdr[k]

    if bunit is not None:
        bunit2 = bunit
    else:
        bunit2 = hdr["BUNIT"]

    # There's either "RESTFRQ" or "RESTFREQ".
    if "RESTFRQ" in hdr:
        output_header["RESTFRQ"] = hdr["RESTFRQ"]
    else:
        output_header["RESTFREQ"] = hdr["RESTFREQ"]

    output_header["NAXIS"] = 2

    target_wcs = WCS(output_header)
    cel_header = target_wcs.to_header()

    # Add other information.
    output_header["BUNIT"] = bunit2
    cel_header.set("BUNIT", bunit2,
                   comment="Units of spectral brightness for pixel value")
    output_header["BMAJ"] = hdr["BMAJ"]
    cel_header.set("BMAJ", hdr["BMAJ"],
                   comment="[deg] Beam major axis")
    output_header["BMIN"] = hdr["BMIN"]
    cel_header.set("BMIN", hdr["BMIN"],
                   comment="[deg] Beam minor axis")
    output_header["BPA"] = hdr["BPA"]
    cel_header.set("BPA", hdr["BPA"],
                   comment="[deg] Beam position angle")

    return output_header, target_wcs, cel_header


def setup_header(map_size, map_centre, num_channels, vel_min, vel_max, vel_inc,
                 rest_freq, bunit, pix_size=None, beam_size=None, pix_ratio=4.0,
                 map_padding=0.02, projection="galactic", limit=1e6, dims=3):
    if pix_size is None:
        if beam_size is None:
            raise ValueError("Neither pix_size nor beam_size were specified; at least one is need!")
        pix_size = beam_size / pix_ratio
    else:
        beam_size = pix_size * pix_ratio

    num_pix = (int((map_size[0] + map_padding) / pix_size),
               int((map_size[1] + map_padding) / pix_size))
    if num_pix[0] * num_pix[1] > limit:
        logging.ERROR("Too many pixels to grid! Exiting.")
        exit(1)

    if projection.lower() == "galactic":
        ctype1 = "GLON-SIN"
        ctype2 = "GLAT-SIN"
    elif projection.lower() == "j2000":
        ctype1 = "RA---SIN"
        ctype2 = "DEC--SIN"

    header = {
        "RESTFREQ": rest_freq * 1e9,

        "NAXIS1": num_pix[0],
        "CRPIX1": num_pix[0] / 2,
        "CTYPE1": ctype1,
        "CUNIT1": "deg",
        "CDELT1": -pix_size,
        "CRVAL1": map_centre[0],

        "NAXIS2": num_pix[1],
        "CRPIX2": num_pix[1] / 2,
        "CTYPE2": ctype2,
        "CUNIT2": "deg",
        "CDELT2": pix_size,
        "CRVAL2": map_centre[1],

        "SPECSYS": "LSRK",
    }

    if dims == 3:
        header.update({
            "NAXIS": 3,
            "NAXIS3": num_channels,
            "CRPIX3": num_channels / 2,
            "CTYPE3": "VRAD",
            "CUNIT3": "km/s",
            "CDELT3": vel_inc,
            "CRVAL3": (vel_min + vel_max) / 2,
        })
    else:
        header.update({"NAXIS": 2})

    target_wcs = WCS(header)
    cel_header = target_wcs.to_header()

    if dims == 3:
        # Repair the velocity axis.
        cel_header.set("CTYPE3", "VELO-LSR")

    # Add other information.
    header["BUNIT"] = bunit
    cel_header.set("BUNIT", bunit,
                   comment="Units of spectral brightness for pixel value")
    header["BMAJ"] = beam_size
    cel_header.set("BMAJ", beam_size,
                   comment="[deg] Beam major axis")
    header["BMIN"] = beam_size
    cel_header.set("BMIN", beam_size,
                   comment="[deg] Beam minor axis")
    header["BPA"] = 0
    cel_header.set("BPA", 0,
                   comment="[deg] Beam position angle")

    return header, target_wcs, cel_header


def _write_fits(filename, hdr, d, overwrite=True):
    if os.path.exists(filename):
        if overwrite:
            if os.path.isfile(filename):
                os.remove(filename)
            else:
                shutil.rmtree(filename)
        else:
            logger.info("Not overwriting existing file %s", filename)
            return

    logger.info("Writing %s ... ", filename)
    sys.stdout.flush()

    pyfits.writeto(filename,
                   header=hdr,
                   data=d)

    logger.info("done.")


def _sanity(filename, data, data_hdr, write, output_filename):
    # One of filename or (data and data_hdr) must be specified.
    if filename is None and data is None:
        raise ValueError("Neither a file nor data were supplied!")
    elif data is not None and data_hdr is None:
        raise ValueError("When providing data, a header must also be supplied!")

    # If we're writing, check that the output_filename is specified.
    if write and data is not None and output_filename is None:
        raise ValueError("When providing data and writing a moment map, an output filename must also be supplied!")

    # Input files must have a fits extension.
    if filename is not None:
        _, ext = os.path.splitext(filename)
        if ext.lower() != ".fits":
            raise ValueError("Input file does not have a .fits file extension!")


def moment_minus2(filename=None, data=None, data_hdr=None, write=False,
                  output_filename=None, overwrite=True):
    """
    Given a fits file containing a cube of data, or a three-dimensional numpy
    array, produce a map of peak intensity the same way that MIRIAD's moment
    task does.

    MIRIAD does a funky "three-point quadratic fit" to yield the peak
    intensity. If you want the *actual* peak intensity, then
    `np.max(self.data_cube, axis=0)` is enough.
    """

    # Sanity checking.
    _sanity(filename, data, data_hdr, write, output_filename)

    if filename is not None:
        if output_filename is None:
            output_filename_base, _ = os.path.splitext(filename)
            output_filename = output_filename_base + ".mom-2.fits"
        hdu = pyfits.open(filename)
        data = hdu[0].data
        data_hdr = hdu[0].header

    # Check the presence of any existing files.
    if write and os.path.exists(output_filename):
        if overwrite:
            os.remove(output_filename)
        else:
            logger.info("Not overwriting existing file %s.", output_filename)
            return

    d = data
    peak = np.max(d, axis=0)
    i = np.argmax(d, axis=0)
    # https://stackoverflow.com/questions/42519475/python-numpy-argmax-to-max-in-multidimensional-array
    I, J = np.indices(i.shape)

    # Edge cases.
    mask1 = i == 0
    mask2 = i + 1 == d.shape[0]
    mask = ~mask1 & ~mask2
    i[~mask] = 1
    a = 0.5 * (d[i + 1, I, J] + d[i - 1, I, J]) - d[i, I, J]
    b = 0.5 * (d[i + 1, I, J] - d[i - 1, I, J])
    a[~mask] = 0
    b[~mask] = 0

    xpeak = np.zeros_like(peak)
    amask = a != 0
    xpeak[amask] = -b[amask] / (2.0 * a[amask])

    data_mom_minus2 = peak + xpeak * b / 2.0
    _, _, hdr = fits_cube_hdr2fits_map_hdr(data_hdr)
    if write:
        _write_fits(output_filename, hdr, data_mom_minus2, overwrite=overwrite)

    return data_mom_minus2, hdr


def moment_0(filename=None, data=None, data_hdr=None, write=False,
             output_filename=None, overwrite=True):
    """
    Given a fits file containing a cube of data, or a three-dimensional numpy
    array, produce a map of peak intensity the same way that MIRIAD's moment
    task does.
    """

    # Sanity checking.
    _sanity(filename, data, data_hdr, write, output_filename)

    if filename is not None:
        if output_filename is None:
            output_filename_base, _ = os.path.splitext(filename)
            output_filename = output_filename_base + ".mom0.fits"
        hdu = pyfits.open(filename)
        data = hdu[0].data
        data_hdr = hdu[0].header

    _, _, hdr = fits_cube_hdr2fits_map_hdr(data_hdr, bunit="K." + data_hdr["CUNIT3"])
    data_mom_0 = np.sum(data, axis=0) * abs(data_hdr["CDELT3"])
    if write:
        _write_fits(output_filename, hdr, data_mom_0, overwrite=overwrite)

    return data_mom_0, hdr
