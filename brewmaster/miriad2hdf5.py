import os
import re
import logging

import numpy as np
from astropy.table import Table
from astropy.time import Time

import aipy


logger = logging.getLogger(__name__)


def derive_velocities(p):
    # Same code as MIRIAD's uvlist.for, but edited with Maxim Voronkov's "dopdet".
    # 'p' for "parameters".
    c = 299792458
    beta = (p["veldop"] - p["vsource"]) / c * 1000
    dopdet = np.sqrt(1 - beta**2) / (1 - beta)

    p["veldop"] -= p["vsource"]
    p["vinc"] = 0.001 * c * p["sdf"] / p["restfreq"] * dopdet
    p["vstart"] = 0.001 * c * (1 - p["sfreq"] * dopdet / p["restfreq"])
    p["vend"] = p["vstart"] - p["vinc"] * (p["nschan"] - 1)


def datetime2miriad(dates):
    return_list = []
    for x in dates:
        fractional = x.strftime("%f")
        formatted = x.strftime("%y%b%d:%H:%M:%S.%f")[:-len(fractional) + 1]
        return_list.append(np.string_(formatted))
    return return_list


class MiriadFile:
    def __init__(self, filename, hdf5_filename=None, antenna=None):
        self.filename = re.sub(r"/*$", "", filename)

        if antenna is not None:
            self.antenna = antenna
        else:
            self.antenna = int(re.search(r"ant(\d)", filename).group(1))

        if hdf5_filename is not None:
            self.hdf5_filename = hdf5_filename
        else:
            self.hdf5_filename = self.filename + ".hdf5"

    def unpack(self):
        self.uv = aipy.miriad.UV(self.filename)
        self.num_channels = self.uv["nschan"]
        # We assume that all scans have the same number of polarisations.
        self.num_pols = self.uv["npol"]

        # How are you meant to know how many scans are in the file?
        count = 0
        for i, (_, _) in enumerate(self.uv.all()):
            if i == 0:
                self.telescope = self.uv["telescop"]
                self.rest_freq = self.uv["restfreq"]
                # We assume that all sdf and sfreq values are the same.
                self.sdf = self.uv["sdf"]
                self.sfreq = self.uv["sfreq"]
            if i % self.num_pols == 0:
                count += 1
        self.uv.rewind()
        logger.info("%s contains %s scans.", self.hdf5_filename, count)

        self.d = {
            "source": np.empty(count, dtype="S9"),
            "date": np.empty(count, dtype="float64"),
            "ra": np.empty(count, dtype="float64"),
            "dec": np.empty(count, dtype="float64"),
            "veldop": np.empty(count, dtype="float64"),
            "vsource": np.empty(count, dtype="float64"),
            "xx_tsys": np.empty(count, dtype="float32"),
            "yy_tsys": np.empty(count, dtype="float32"),
            "elev": np.empty(count, dtype="float32"),
            "temp": np.empty(count, dtype="float32"),
            "pressure": np.empty(count, dtype="float32"),
            "humidity": np.empty(count, dtype="float32"),
            "jyperk": np.empty(count, dtype="float32"),
            "xx": np.empty((count, self.num_channels), dtype="float32"),
            "yy": np.empty((count, self.num_channels), dtype="float32"),
            "raw_file": np.empty(count, dtype="S21"),
        }

        # Increment through all scans. We're only interested in XX and YY
        # pols, so we keep count of which pol we're up to, ignoring the
        # others.
        source_count = 0
        for i, (_, data) in enumerate(self.uv.all()):
            if i % self.num_pols == 0:
                # Populate pre-allocated arrays.
                self.d["source"][source_count] = self.uv["source"]
                self.d["date"][source_count] = self.uv["time"]
                self.d["ra"][source_count] = self.uv["ra"] * 180 / np.pi
                self.d["dec"][source_count] = self.uv["dec"] * 180 / np.pi
                self.d["veldop"][source_count] = self.uv["veldop"]
                self.d["vsource"][source_count] = self.uv["vsource"]
                self.d["xx_tsys"][source_count] = self.uv["xtsys"][self.antenna - 1]
                self.d["yy_tsys"][source_count] = self.uv["ytsys"][self.antenna - 1]
                self.d["elev"][source_count] = self.uv["antel"]
                self.d["temp"][source_count] = self.uv["airtemp"]
                self.d["pressure"][source_count] = self.uv["pressmb"]
                self.d["humidity"][source_count] = self.uv["relhumid"]
                self.d["jyperk"][source_count] = self.uv["jyperk"]
                self.d["raw_file"][source_count] = self.uv["name"]
                self.d["xx"][source_count, :] = np.real(data)
            elif i % self.num_pols == 1:
                self.d["yy"][source_count, :] = np.real(data)
                source_count += 1

        # Convert the times into the human-readable MIRIAD format.
        self.d["date"] = datetime2miriad(Time(self.d["date"], format="jd").datetime)

        self.table = Table(self.d,
                           names=("source",
                                  "date",
                                  "ra",
                                  "dec",
                                  "veldop",
                                  "vsource",
                                  "xx_tsys",
                                  "yy_tsys",
                                  "elev",
                                  "temp",
                                  "pressure",
                                  "humidity",
                                  "jyperk",
                                  "xx",
                                  "yy",
                                  "raw_file"),
                           meta={"telescope": self.telescope,
                                 "rest_freq": self.rest_freq,
                                 "antenna": self.antenna,
                                 "num_channels": self.num_channels,
                                 "sdf": self.sdf,
                                 "sfreq": self.sfreq,
                                 })

    def write(self):
        logger.info("Saving %s ...", self.hdf5_filename)
        if os.path.exists(self.hdf5_filename):
            os.remove(self.hdf5_filename)
        self.table.write(self.hdf5_filename, path="raw", compression=True, overwrite=True)
        logger.info("done.")
