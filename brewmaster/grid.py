import os
import logging

import numpy as np
from astropy.table import Table
import cygrid

from brewmaster.fits import setup_header, _write_fits, moment_minus2, moment_0


logger = logging.getLogger(__name__)


def rest_freq2beam_size(rest_freq, diameter=22.0):
    """
    From a rest frequency specified in gigahertz (and optionally a telescope
    diameter specified in metres, if not the ATCA), calculate the beam size.
    """
    c = 299792458
    return np.rad2deg(c / (rest_freq * 1e9 * diameter))


class Grid:
    def __init__(self, inp, projection="galactic", bunit=None,
                 rest_freq=None, path="data", verbosity=False):
        """
        Set up the parameters required for gridding.
        """

        # Input handling.
        # The input can be a string for a filepath, or an astropy table object.
        if isinstance(inp, str):
            self.filename = inp
            self.basename = os.path.basename(inp)
            self.table = Table.read(inp, path=path)
        elif isinstance(inp, Table):
            self.filename = None
            self.table = inp
        else:
            raise ValueError("Input unrecognised!")

        if rest_freq is None:
            self.rest_freq = self.table.meta["rest_freq"]
        else:
            self.rest_freq = rest_freq

        self.verbosity = verbosity

        self.projection = projection.lower()
        if self.projection == "galactic":
            self.x = np.array(self.table["glon"], dtype=float)
            self.y = np.array(self.table["glat"], dtype=float)
        elif self.projection == "j2000":
            self.x = np.array(self.table["ra"], dtype=float)
            self.y = np.array(self.table["dec"], dtype=float)
        else:
            raise ValueError("Unknown projection specified!")

        # Try to determine the appropriate BUNIT.
        # If user-specified, simply use this. Otherwise, try to determine it
        # from the metadata, but if not present, assume 'K'.
        if bunit is not None:
            self.bunit = bunit
        else:
            try:
                if self.table.meta["extended"]:
                    self.bunit = 'K'
                elif not self.table.meta["extended"]:
                    self.bunit = "Jy"
            except KeyError:
                self.bunit = 'K'

        self.map_size = (np.max(self.x) - np.min(self.x),
                         np.max(self.y) - np.min(self.y))
        self.map_centre = (np.mean([np.max(self.x), np.min(self.x)]),
                           np.mean([np.max(self.y), np.min(self.y)]))
        self.beam_size = rest_freq2beam_size(self.rest_freq)
        self.num_channels = len(self.table["spectrum"][0])

        self._mom_minus2 = False
        self._mom_0 = False

    def setup_fits_header(self, pix_size=None, pix_ratio=4.0, map_padding=0.02,
                          bunit=None, limit=1e6, dims=3):
        if pix_size is None:
            pix_size = self.beam_size / pix_ratio
        self.pix_size = pix_size
        self.map_padding = map_padding
        self.num_pix = (int((self.map_size[0] + map_padding) / self.pix_size),
                        int((self.map_size[1] + map_padding) / self.pix_size))

        self.header, self.target_wcs, self.cel_header = \
            setup_header(self.map_size, self.map_centre, self.num_channels,
                         self.table.meta["vel_min"], self.table.meta["vel_max"],
                         self.table.meta["vel_inc"], self.rest_freq, self.bunit,
                         pix_size=self.pix_size, map_padding=map_padding,
                         projection=self.projection, limit=limit, dims=dims)

    def grid(self,
             kernel_type="gauss1d",
             kernel_size_sigma=0.4,
             kernel_support=3,
             hpx_maxres=0.5):
        self.kernel_size_sigma = self.beam_size * kernel_size_sigma
        self.kernel_support = self.kernel_size_sigma * kernel_support
        self.hpx_maxres = self.kernel_size_sigma * hpx_maxres

        gridder = cygrid.WcsGrid(self.header)
        gridder.set_kernel(kernel_type,
                           (self.kernel_size_sigma,),
                           self.kernel_support,
                           self.hpx_maxres)
        gridder.grid(self.x, self.y, self.table["spectrum"])
        # There always seems to be a warning about true_divide when performing
        # the .get_datacube method below; ignore that.
        with np.errstate(invalid="ignore", divide="ignore"):
            self.data_cube = gridder.get_datacube()

    def mom_minus2(self):
        self.data_mom_minus2, self.mom_minus2_header = moment_minus2(data=self.data_cube,
                                                                     data_hdr=self.header)
        self._mom_minus2 = True

    def mom_0(self):
        self.data_mom_0, self.mom_0_header = moment_0(data=self.data_cube,
                                                      data_hdr=self.header)
        self._mom_0 = True

    def write(self, output_filename=None, overwrite=True):
        if output_filename is None:
            if self.filename is not None:
                self.output_filename_base, _ = os.path.splitext(os.path.basename(self.filename))
            else:
                raise ValueError("No output filename specified!")
        else:
            self.output_filename_base, _ = os.path.splitext(output_filename)

        f = self.output_filename_base + ".fits"
        _write_fits(f, self.cel_header, self.data_cube, overwrite=overwrite)

        if self._mom_minus2:
            f = self.output_filename_base + ".mom-2.fits"
            _write_fits(f, self.mom_minus2_header, self.data_mom_minus2, overwrite=overwrite)

        if self._mom_0:
            f = self.output_filename_base + ".mom0.fits"
            _write_fits(f, self.mom_0_header, self.data_mom_0, overwrite=overwrite)
