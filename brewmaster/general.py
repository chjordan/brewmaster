import os
import matplotlib.pyplot as plt


def trim_edges(table_object, edge_ignore):
    t = table_object.table
    t["xx"] = t["xx"][:, edge_ignore:-edge_ignore]
    t["yy"] = t["yy"][:, edge_ignore:-edge_ignore]


def dump_spectrum_image(spectrum, title, directory):
    if not os.path.exists(directory):
        os.mkdir(directory)
    if not os.path.exists("%s/%s.png" % (directory, title)):
        _, ax = plt.subplots(1, figsize=(15, 3))
        ax.plot(spectrum)
        ax.set_xlim(-5, len(spectrum) + 5)
        ax.set_title(title)
        plt.tight_layout()
        plt.savefig("%s/%s.jpg" % (directory, title))
        plt.close()
