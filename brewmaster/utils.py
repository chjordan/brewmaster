import logging
import numpy as np
from scipy import ndimage


logger = logging.getLogger(__name__)


def interpolate_over_line(spectrum, channel_range, edge_channels,
                          median_width=25, padding=3, poly_order=2):
    """
    From a channel range containing a line, interpolate a polynomial using the
    median of the channels either side of the line.
    """

    if isinstance(channel_range, (int, np.int64)):
        channel_range = [channel_range, channel_range]

    x1, x2 = channel_range[0], channel_range[1] + 1
    edge_1 = max(x1 - median_width - padding, edge_channels[0])
    edge_2 = min(x2 + median_width + padding, edge_channels[1])

    range_1 = np.arange(edge_1, edge_1 + median_width)
    range_2 = np.arange(edge_2 - median_width, edge_2)
    interp_range = np.concatenate((range_1, range_2)).astype(int)
    poly_fit = np.poly1d(np.polyfit(interp_range, np.array(spectrum)[interp_range], poly_order))

    replace_edge_1 = int(max(x1 - padding, edge_channels[0]))
    replace_edge_2 = int(min(x2 + padding, edge_channels[1]))
    spectrum[replace_edge_1:replace_edge_2] = poly_fit(np.arange(replace_edge_1, replace_edge_2))


def difference_scan(spectrum, smooth_window):
    smoothed = ndimage.gaussian_filter1d(spectrum, smooth_window)
    return np.abs(spectrum - smoothed)


def detect_and_flag_spikes(spectrum, threshold, absolute=True,
                           scan_name=None, median_width=3, smooth_window=3,
                           edge_ignore=0, padding=0):
    if edge_ignore == 0:
        d_spectrum = difference_scan(spectrum, smooth_window)
    else:
        d_spectrum = difference_scan(spectrum, smooth_window)[edge_ignore:-edge_ignore]

    # Keep track of how many times we've looped in the while loop, to prevent
    # infinite loops. Also keep track of the flagged channels.
    i = 0
    flagged_channels = []

    if absolute:
        condition = np.max(d_spectrum) > threshold
    else:
        condition = np.max(d_spectrum) / np.std(d_spectrum) > threshold
    while condition:
        x = d_spectrum.argmax()
        flagged_channels.append(x + edge_ignore)
        interpolate_over_line(spectrum, x + edge_ignore,
                              edge_channels=[0, len(spectrum) - 1],
                              median_width=median_width, padding=0)
        if scan_name is not None:
            logger.debug("Scan %s flagged channel %s" % (scan_name, x + edge_ignore))
        else:
            logger.debug("Flagged channel %s" % (x + edge_ignore))

        # Regenerate the d_spectrum, and re-evaluate the condition for the while
        # loop.
        if edge_ignore == 0:
            d_spectrum = difference_scan(spectrum, smooth_window)
        else:
            d_spectrum = difference_scan(spectrum, smooth_window)[edge_ignore:-edge_ignore]
        if absolute:
            condition = np.max(d_spectrum) > threshold
        else:
            condition = np.max(d_spectrum) / np.std(d_spectrum) > threshold

        i += 1
        if i > 10:
            # If the while loop iterates too many times, it's likely that there's
            # something wrong with the spectrum ("CABB fuzzy"). The next section
            # of code should pick it up and flag it.
            if scan_name is not None:
                logger.debug("Scan %s has too many bad channels" % scan_name)
            else:
                logger.debug("Scan has too many bad channels")
            break

    return flagged_channels
