import os
import numpy as np
from scipy.interpolate import InterpolatedUnivariateSpline as spline

from brewmaster.constants import K, H, C, nu0, a1, a2, a3, a4, a5, a6, mnu0, b1, b2, b3


def jyperk(freq, telescope):
    """
    Taken from the help of MIRIAD's uvgen.
    """

    if telescope == "ATCA":
        D = 22.0
        # The help text of uvgen specifies e to be 0.88, but we use 1.0 as 0.88
        # is a value associated with the old ATCA correlator's digitisation
        # error.
        e = 1.0

    A = np.pi / 4.0 * D**2
    eta = e * efficiency(freq, telescope)
    return 2 * K / (eta * A) * 1e26


def efficiency(freq, telescope):
    if telescope == "ATCA":
        f = [900.0,    1200.0,   1500.0,   1800.0,  2100.0,  2300.0,  2500.0,  4400.0,  5900.0,
             7400.0,   8800.0,   10600.0,  16000.0, 16500.0, 17000.0, 17500.0, 18000.0, 18500.0,
             19000.0,  19500.0,  20000.0,  20500.0, 21000.0, 21500.0, 22000.0, 22500.0, 23000.0,
             23500.0,  24000.0,  24500.0,  25000.0, 25400.0, 30000.0, 31000.0, 32000.0, 33000.0,
             34000.0,  35000.0,  36000.0,  37000.0, 38000.0, 39000.0, 40000.0, 41000.0, 42000.0,
             43000.0,  44000.0,  45000.0,  46000.0, 47000.0, 48000.0, 49000.0, 50000.0, 83781.1,
             85556.2,  86834.3,  88680.5,  90526.6, 91946.7, 94005.9, 95852.1, 97272.2, 98976.3,
             100254.4, 102200.0, 102300.0, 106432.0]
        e = [0.57,     0.57,     0.60,     0.53,    0.43,    0.42,    0.44,    0.65,    0.72,
             0.65,     0.64,     0.65,     0.58,    0.62,    0.63,    0.65,    0.67,    0.70,
             0.68,     0.64,     0.64,     0.60,    0.53,    0.55,    0.54,    0.51,    0.51,
             0.53,     0.49,     0.49,     0.46,    0.47,    0.60,    0.60,    0.60,    0.60,
             0.60,     0.60,     0.60,     0.60,    0.60,    0.60,    0.60,    0.59,    0.58,
             0.57,     0.56,     0.55,     0.54,    0.53,    0.52,    0.51,    0.50,    0.3297,
             0.3065,   0.3020,   0.2856,   0.2689,  0.2670,  0.2734,  0.2727,  0.2521,  0.2403,
             0.2336,   0.2322,   0.14,     0.14]
        spl = spline(f, e, ext="zeros")

    return spl(freq)


def pvapsat(t):
    """
    Determine the saturation pressure of water vapour. Taken from MIRIAD.

    Parameters
    ----------
    t : int, float or array_like
        Air temperature [Kelvin].

    Returns
    -------
    out : float or ndarray
        Saturation pressure.
    """

    if isinstance(t, (int, float)):
        if t > 215:
            theta = 300.0 / t
            return 1e5 / (41.51 * (theta**-5) * (10**(9.384 * theta - 10.0)))
        else:
            return 0.0
    else:
        r = np.zeros_like(t, dtype=np.float)
        indices = np.where(t > 215)
        sub = t[indices]
        theta = 300.0 / sub
        r[indices] = 1e5 / (41.51 * (theta**-5) * (10**(9.384 * theta - 10.0)))
        return r


def refdry(nu, t, p_dry, p_vap):
    """
    Determine the complex refractivity of the dry components of the atmosphere.
    Taken from MIRIAD.

    Parameters
    ----------
    nu : int or float
        Observing frequency [Hz]
    t : ndarray
        Air temperature [Kelvin]
    p_dry : int, float or ndarray
        Partial pressure of dry components [Pa]
    p_vap : int, float or ndarray
        Partial pressure of water vapour [Pa]

    Returns
    -------
    out : float or ndarray
        Complex refractivity of the dry components of the atmosphere.
    """

    # Convert to the units of Liebe.
    theta = 300.0 / t
    e = 0.001 * p_vap
    p = 0.001 * p_dry
    f = nu * 1e-9

    ap = 1.4e-10 * (1.0 - 1.2e-5 * f ** 1.5)
    gamma0 = 5.6e-3 * (p + 1.1 * e) * theta ** 0.8
    nr = (2.588 * p * theta + 3.07e-4 * (1.0 / (1.0 + (f / gamma0) ** 2) - 1.0) * p * theta**2)
    ni = ((2.0 * 3.07e-4 / (gamma0 * (1.0 + (f / gamma0) ** 2) * (1.0 + (f / 60.0)**2)) +
          ap * p * theta**2.5) * f * p * theta**2)

    # Sum the contributions of the lines.

    # S
    e1 = np.exp(np.outer((1.0 - theta), a2).reshape((t.shape[0], t.shape[1], -1)))  # [temps, layers, a1.size]
    e2 = (e1 * a1).transpose((2, 0, 1))  # [a1.size, temps, layers]
    S = (e2 * p * theta**3).transpose((1, 2, 0))  # [temps, layers, a1.size]

    # gamma
    e1 = np.power.outer(theta, (0.8 - a4)).reshape((t.shape[0], t.shape[1], -1))  # [temps, layers, a1.size]
    e2 = (e1 * a3).transpose((2, 0, 1))  # [a1.size, temps, layers]
    gamma = (e2 * p + 1.1 * e * theta).transpose((1, 2, 0))  # [temps, layers, a1.size]

    # delta
    e1 = np.power.outer(theta, a6).reshape((t.shape[0], t.shape[1], -1))  # [temps, layers, a1.size]
    e2 = (e1 * a5).transpose((2, 0, 1))  # [temps, a1.size, layers]
    delta = (e2 * p).transpose((1, 2, 0))  # [temps, layers, a1.size]

    x = (nu0 - f)**2 + gamma**2
    y = (nu0 + f)**2 + gamma**2
    z = nu0 + gamma**2 / nu0

    # nr and ni
    e1 = (z - f) / x
    e2 = (z + f) / y
    e3 = delta * (1.0 / x - 1.0 / y) * gamma * f / nu0
    nr += np.sum(S * (e1 + e2 - 2 / nu0 + e3), axis=2)

    e1 = (1.0 / x + 1.0 / y) * gamma * f
    e2 = e1 / nu0
    ni += np.sum(S * (e2 - delta * ((nu0 - f) / x + (nu0 + f) / y) * f / nu0), axis=2)

    return nr, ni


def refvap(nu, t, p_dry, p_vap):
    """
    Determine the complex refractivity of the water vapour monomers.
    Taken from MIRIAD.

    Parameters
    ----------
    nu : int or float
        Observing frequency [Hz]
    t : ndarray
        Air temperature [Kelvin]
    p_dry : int, float or ndarray
        Partial pressure of dry components [Pa]
    p_vap : int, float or ndarray
        Partial pressure of water vapour [Pa]

    Returns
    -------
    out : ndarray
        Complex refractivity of the dry components of the atmosphere.
    """

    # Convert to the units of Liebe.
    theta = 300.0 / t
    e = 0.001 * p_vap
    p = 0.001 * p_dry
    f = nu * 1e-9

    nr = (2.39 * e * theta + 41.6 * e * theta**2 + 6.47e-6 * f ** 2.05 * e * theta ** 2.4)
    ni = ((0.915 * 1.40e-6 * p + 5.41e-5 * e * theta**3) * f * e * theta ** 2.5)

    # Sum the contributions of the lines.

    # S
    e1 = np.exp(np.outer((1.0 - theta), b2)).reshape((t.shape[0], t.shape[1], -1))  # [temps, layers, b1.size]
    e2 = (e * theta**3.5).reshape((t.shape[0], t.shape[1], -1))  # [temps, layers, b1.size]
    S = b1 * e2 * e1

    gamma = np.outer((p * theta**0.8 + 4.80 * e * theta), b3).reshape((t.shape[0], t.shape[1], -1))  # [temps, layers, b1.size]
    x = (mnu0 - f)**2 + gamma**2
    y = (mnu0 + f)**2 + gamma**2
    z = (mnu0 + gamma**2 / mnu0)

    # nr and ni
    e1 = (z - f) / x
    e2 = (z + f) / y
    nr += np.sum(S * (e1 + e2 - 2 / mnu0), axis=2)
    ni += np.sum(S * ((1.0 / x + 1.0 / y) * gamma * f / mnu0), axis=2)

    return nr, ni


class Obs:
    def __init__(self, freq, temp, pressure, humidity, elev, dec, telescope):
        """
        For a set of observations, which only applies to a single frequency,
        using the air temperatures, pressures, humidities and elevations,
        determine the system temperatures and antenna efficiency.

        Note that it would be pretty simple to solve for multiple frequencies
        simultaneously, but for now we assume only a single frequency.

        Parameters
        ----------
        freq : float
            Frequency [MHz]
        temp : int, float or array_like
            Observatory air temperature [Kelvin]
        pressure : int, float or array_like
            Observatory air pressure [Pa]
        humidity : float or array_like
            Observatory humidity [fraction, i.e. 0.5 for 50%]
        elev : float or array_like
            Source elevation [degrees]
        dec : int, float or array_like
            Source declination [degrees]
        """

        # Set telescope attributes.
        if telescope == "ATCA":
            self.telescope = telescope
            self.latitude = np.deg2rad(-30.31288472)
            self.diameter = 22.0
        else:
            raise ValueError("Telescope (%s) not recognised!" % telescope)

        self.freq = freq

        # More telescope-dependent variables.
        self.efficiency = efficiency(self.freq, self.telescope)
        self.determine_intrinsic_tsys()
        self.surface_area = np.pi * (self.diameter / 2)**2

        # Assign the remainder of the inputs.
        def to_array(x):
            if isinstance(x, (int, float)):
                return np.array([x], dtype=float)
            elif isinstance(x, list):
                return np.array(temp, dtype=float)
            else:
                return x

        self.temp = to_array(temp)
        self.pressure = to_array(pressure)
        self.humidity = to_array(humidity)
        self.elev = np.deg2rad(to_array(elev))
        self.dec = np.deg2rad(to_array(dec))

    def _atmos_refract(self, t, z, n_layers):
        """
        Compute refractive index for an atmosphere. Determine the sky brightness
        and excess path lengths for a parallel slab atmosphere. Liebe's model
        (1985) is used to determine the complex refractive index of air. Taken
        from MIRIAD.

        Parameters
        ----------
        n_layers : int
            The number of atmospheric layers.
        t : array_like
            Air temperature of the layers. T[0] is the temperature at the lowest
            layer [Kelvin]
        z : array_like
            Height of the layers [m]

        Returns
        -------
        Sky brightness temperature and opacity.
        """

        T0 = 2.7  # CMB temperature
        f = self.freq * 1e6  # The following expects the frequency in Hz

        tau = 0.0
        Tb = H * f / (K * (np.exp(H * f / (K * T0)) - 1))
        snell = np.sin(self.elev)

        n_dry_r, n_dry_i = refdry(f, t[:, 1:], self.p_dry[:, 1:], self.p_vap[:, 1:])
        n_vap_r, n_vap_i = refvap(f, t[:, 1:], self.p_dry[:, 1:], self.p_vap[:, 1:])
        nr = 1 + (n_dry_r + n_vap_r) * 1e-6
        ni = (n_dry_i + n_vap_i) * 1e-6
        for i in range(n_layers, 0, -1):
            # The following looks like calculating a gradient, but it's not quite
            # right. I've left it, because np.gradient would produce a different
            # result.
            if (i == 1):
                dz = 0.5 * (z[1] - z[0])
            elif (i == n_layers):
                dz = 0.5 * (z[n_layers] - z[n_layers - 1])
            else:
                dz = 0.5 * (z[i + 1] - z[i - 1])

            ell = (dz * nr.T / np.sqrt(nr.T**2 + snell**2 - 1.0))[i - 1].T
            dtau = (ell * 4.0 * np.pi * f / C * ni.T)[i - 1].T
            Tb = (Tb - t[:, i]) * np.exp(-dtau) + t[:, i]
            tau += dtau

        self.tau = tau
        self.sky_brightness_temperature = Tb

    def opacity(self, n_layers=50):
        M = 28.96e-3  # ???
        R = 8.314     # Ideal gas constant
        g = 9.81      # Acceleration due to gravity

        d = 0.0065       # temperature lapse rate [K/m]
        z0 = 1540.0      # water vapour scale height [m]
        z_max = 10000.0  # max altitude of model atmosphere [m]

        z = np.linspace(0, z_max, n_layers + 1)

        e1 = 1 + np.outer(d / self.temp, z)
        t = (self.temp / e1.T).T

        e1 = -M * g / (R * self.temp) * (z + np.outer(z**2, 0.5 * d / self.temp).T).T
        p = (self.pressure * np.exp(e1)).T

        p_vap = np.outer(self.humidity * pvapsat(t[:, 0]), np.exp(-z / z0))

        self.p_vap = np.minimum(p_vap, pvapsat(t))
        self.p_dry = p - p_vap

        self._atmos_refract(t, z=z, n_layers=n_layers)

        self.transmissivity = np.exp(-self.tau)

    def determine_intrinsic_tsys(self):
        f = self.freq / 1000
        if self.telescope == "ATCA":
            # If the frequency is not within the specified range, raise an error.
            if f >= 3.905 and f <= 11.951:
                filename = os.path.dirname(__file__) + "/systemps/ca02_4cm_y_polarisation.avg"
            elif f >= 29.077 or f <= 50.923:
                filename = os.path.dirname(__file__) + "/systemps/ca02_7mm.avg"
            elif f >= 83.857 or f <= 104.785:
                filename = os.path.dirname(__file__) + "/systemps/nominal_3mm.avg"
            else:
                raise ValueError("No gain data exists for given frequency ({0} GHz!)".format(f))

        freq, tsys = np.loadtxt(filename, unpack=True)
        spl = spline(freq, tsys)
        with np.errstate(under="ignore"):
            self.intrinsic_tsys = 10**(spl(f))

    def determine_tsys(self):
        """
        Given the weather, determine what the measured Tsys should be.
        """

        # If some attributes do not exist, generate them.
        # This saves the user from running methods manually.
        try:
            self.tau
        except AttributeError:
            self.opacity()

        # The elevation at this hour angle.
        cosha = np.cos(self.elev)
        sinel = np.sin(self.latitude) * np.sin(self.dec) + np.cos(self.latitude) * np.cos(self.dec) * cosha

        # Calculate the excess temperature due to the atmosphere and CMB.
        elFactor = np.exp(-self.tau / sinel)
        cbFactor = 2.7 * elFactor
        ivFactor = 1.0 - elFactor
        atFactor = self.sky_brightness_temperature * ivFactor
        Texcess = atFactor + cbFactor

        Tmeas = self.intrinsic_tsys + Texcess
        TmeasEff = Tmeas / self.efficiency
        self.tsys = TmeasEff
