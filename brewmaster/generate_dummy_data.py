import datetime as dt
import numpy as np
from scipy.signal import gaussian
from astropy.table import Table
from astropy.time import Time
from astropy.coordinates import SkyCoord
import astropy.units as u


def _datetime2miriad(dates):
    return_list = []
    for x in dates:
        fractional = x.strftime("%f")
        formatted = x.strftime("%y%b%d:%H:%M:%S.%f")[:-len(fractional) + 1].encode(encoding="UTF-8")
        return_list.append(formatted)
    return return_list


def generate_distribution(mean, sigma, size, dist):
    if dist == "constant":
        return np.ones(size) * mean
    elif dist == "lognormal":
        return np.random.lognormal(mean=mean,
                                   sigma=sigma,
                                   size=size)
    elif dist == "normal":
        return np.random.normal(mean=mean,
                                sigma=sigma,
                                size=size)
    else:
        raise ValueError("Unrecognised/unimplemented distribution ({}) used.".format(dist))


def shift_sources_too_close_to_map_edge(source_array, map_size, percent_cutoff):
    """
    This doesn't work yet?
    """
    lower_cutoff = map_size * percent_cutoff / 100
    upper_cutoff = map_size * (100 - percent_cutoff) / 100
    print(source_array[source_array < lower_cutoff])
    print(source_array[source_array > upper_cutoff])
    source_array[source_array < lower_cutoff] += int(lower_cutoff)
    source_array[source_array > upper_cutoff] -= int(upper_cutoff)


def make_maser_sky(num_masers=100,
                   brightness_dist="lognormal",
                   brightness_mean=1,
                   brightness_sigma=1,
                   linewidth_dist="constant",
                   linewidth_mean=3,
                   linewidth_sigma=1,
                   resolution=10 / 60 / 60,
                   sky_origin=(333.0, -0.5),
                   sky_dim=(0.5, 0.5),
                   num_channels=1024,
                   maser_edge_avoid_percent=2):
    """
    Make a sky (cube) of data, containing only a few Gaussian-profiled masers.
    The intention of the cube is to add noise to it, to simulate a real sky.
    Also return the parameters of the masers (position, brightness, line width)
    to analyse the performance of brewmaster. All units in degrees!
    """

    brightness = generate_distribution(brightness_mean,
                                       brightness_sigma,
                                       num_masers,
                                       brightness_dist)
    linewidth = generate_distribution(linewidth_mean,
                                      linewidth_sigma,
                                      num_masers,
                                      linewidth_dist)

    sky_size = (len(np.arange(sky_origin[0], sky_origin[0] + sky_dim[0], resolution)),
                len(np.arange(sky_origin[1], sky_origin[1] + sky_dim[1], resolution)),
                num_channels)
    sky = np.zeros(sky_size)

    spatial_coords = np.random.uniform(low=0,
                                       high=sky[:, :, 0].size,  # Only the spatial coords!
                                       size=num_masers).astype(int)
    maser_spatial_x_coords = spatial_coords // sky.shape[0]
    maser_spatial_y_coords = spatial_coords % sky.shape[0]
    # We can arbitrarily avoid placing masers at the edge of our map. Avoid 2%
    # of the edges.
    # shift_sources_too_close_to_map_edge(maser_spatial_x_coords,
    #                                     sky.shape[0],
    #                                     maser_edge_avoid_percent)
    # shift_sources_too_close_to_map_edge(maser_spatial_y_coords,
    #                                     sky.shape[1],
    #                                     maser_edge_avoid_percent)

    # Provide the Galactic coordinates of the masers.
    maser_spatial_glon_coords = maser_spatial_x_coords / sky.shape[0] * sky_dim[0] + sky_origin[0]
    maser_spatial_glat_coords = maser_spatial_y_coords / sky.shape[1] * sky_dim[1] + sky_origin[1]

    # If a maser is placed at the edge of the channel range, then its emission
    # could appear at either side of the channel range. Avoid the top and bottom
    # 5% of the channel range.
    bottom_spectral_cutoff = num_channels // 20
    top_spectral_cutoff = num_channels * 19 // 20
    maser_spectral_coords = np.random.uniform(low=bottom_spectral_cutoff,
                                              high=top_spectral_cutoff,
                                              size=num_masers).astype(int)

    true_maser_params = {
        "x_coords": maser_spatial_x_coords,
        "y_coords": maser_spatial_y_coords,
        "glon": maser_spatial_glon_coords,
        "glat": maser_spatial_glat_coords,
        "spectral_coords": maser_spectral_coords,
        "brightnesses": brightness,
        "linewidths": linewidth
    }

    # Need to ignore underflows, because we'll otherwise crash, and they don't
    # affect the output.
    with np.errstate(under="ignore"):
        for i, c in enumerate(maser_spectral_coords):
            maser_signal = brightness[i] * gaussian(num_channels, linewidth[i])
            rolled = np.roll(maser_signal,
                             c - num_channels // 2)
            sky[maser_spatial_y_coords[i], maser_spatial_x_coords[i], :] += rolled

    return sky, true_maser_params


def make_metadata(channels, rows, columns, x_extent, y_extent):
    """
    make_metadata is a convenience function that generates observation names,
    dates, positions and velocity data.
    """

    output = {}
    # Create a "reference" observation.
    sources = ["ref".encode(encoding="UTF-8")]
    # Create a "source" observation for every row and column.
    for r in np.arange(1, rows + 1):
        for n in np.arange(1, columns + 1):
            sources.append("m45_%02d%03d".encode(encoding="UTF-8") % (r, n))
    output["source"] = np.array(sources)

    # Generate a date for every observation.
    output["date"] = []
    d = Time(1100000000, format="gps").datetime
    for _ in output["source"]:
        output["date"].append(d)
        d += dt.timedelta(6 / (24 * 3600))
    output["date"] = _datetime2miriad(output["date"])

    # Generate coordinates for every observation.
    glon, glat = np.meshgrid(np.linspace(0, x_extent, rows), np.linspace(0, y_extent, columns))
    glon += 333.0
    glat += -0.5
    glon = glon.reshape((glon.shape[0] * glon.shape[1]))
    glat = glat.reshape((glat.shape[0] * glat.shape[1]))

    # Don't forget the reference.
    glon = np.concatenate(([333.0], glon))
    glat = np.concatenate(([-1.5], glat))

    s = SkyCoord(glon, glat, unit=(u.deg, u.deg), frame="galactic")
    radec = s.transform_to("fk5")
    output["ra"] = radec.ra.deg
    output["dec"] = radec.dec.deg

    # Generate a velocity axis.
    output["vel_start"] = np.zeros_like(output["source"], dtype=np.float32)
    output["vel_end"] = np.ones_like(output["source"], dtype=np.float32) * channels
    output["vel_inc"] = np.ones_like(output["source"], dtype=np.float32)

    output["xx_tsys"] = np.random.normal(loc=50,
                                         scale=2,
                                         size=output["source"].shape)
    output["yy_tsys"] = np.random.normal(loc=50,
                                         scale=2,
                                         size=output["source"].shape)

    # Generate weather parameters.
    output["temp"] = np.ones_like(output["source"]).astype(float) * 23.
    output["pressure"] = np.ones_like(output["source"]).astype(float) * 100.
    output["humidity"] = np.ones_like(output["source"]).astype(float) * 0.5

    # In ATCA data, "jyperk" is an attribute that varies with elevation.
    # Here, we just make it constant, because we're not varying the bandpass
    # gain with elevation.
    output["jyperk"] = np.ones_like(output["source"]).astype(float)

    # Similarly with "jyperk", pretend we only have one elevation.
    output["elev"] = np.ones_like(output["source"]).astype(float) * 60.

    return output


def generate_tables(channels=1024,
                    rows=10,
                    columns=10,
                    number_of_signals=5,
                    number_of_antennas=6,
                    x_extent=0.1,
                    y_extent=0.1,
                    brightness_dist="constant",
                    brightness_value=50,
                    brightness_sigma=2,
                    signal_width=20,
                    signal_edge_avoid=0,
                    path=None,
                    write=True):
    """
    This function creates dummy data for testing with brewmaster.
    The data are arranged as a bunch of "observations" within a table,
    each with its own spectrum, coordinates, date, etc.
    "rows" and "columns" arranges these observations in a grid.
    - channels: the number of spectral channels
    - rows: the number of sources in the "x" direction
    - columns: the number of sources in the "y" direction
    - number_of_signals: inject this many signals randomly into the observations
    - seed: assign a seed value such that the produced results can be consistent.
    - x_extent: the amount of "x" to cover
    - y_extent: the amount of "y" to cover
    - path: write the files to a specified path.
    - write: whether or not to write the astropy tables to disk.
    """

    if brightness_dist == "constant":
        brightness = np.ones(number_of_signals) * brightness_value
    elif brightness_dist == "lognormal":
        brightness = np.random.lognormal(mean=brightness_value,
                                         sigma=brightness_sigma,
                                         size=number_of_signals)
    elif brightness_dist == "normal":
        brightness = np.random.normal(mean=brightness_value,
                                      sigma=brightness_sigma,
                                      size=number_of_signals)
    else:
        raise ValueError("Unrecognised/unimplemented brightness distribution used.")

    meta = make_metadata(channels, rows, columns, x_extent, y_extent)
    sources = meta["source"]
    dates = meta["date"]
    ra = meta["ra"]
    dec = meta["dec"]
    vel_start = meta["vel_start"]
    vel_end = meta["vel_end"]
    vel_inc = meta["vel_inc"]
    temp = meta["temp"]
    pressure = meta["pressure"]
    humidity = meta["humidity"]
    jyperk = meta["jyperk"]
    elev = meta["elev"]

    # Generate the positions of where the signals are going to be injected.
    # We have to start at 1 to avoid the reference spectrum.
    spatial_signal_locations = np.random.uniform(0,
                                                 len(sources),
                                                 size=number_of_signals).astype(int)
    spectral_signal_locations = np.random.uniform(channels * signal_edge_avoid,
                                                  channels * (1 - signal_edge_avoid),
                                                  size=number_of_signals).astype(int)

    xx_tsys_all = np.array_split(np.random.normal(loc=50,
                                                  scale=2,
                                                  size=len(sources) * number_of_antennas),
                                 number_of_antennas)
    yy_tsys_all = np.array_split(np.random.normal(loc=50,
                                                  scale=2,
                                                  size=len(sources) * number_of_antennas),
                                 number_of_antennas)
    xx_all = np.array_split(np.random.normal(loc=10000,
                                             scale=10,
                                             size=(len(sources) * number_of_antennas, channels)),
                            number_of_antennas)
    yy_all = np.array_split(np.random.normal(loc=10000,
                                             scale=10,
                                             size=(len(sources) * number_of_antennas, channels)),
                            number_of_antennas)

    signal_sources = {}
    for i, x in enumerate(spatial_signal_locations):
        try:
            signal_sources[sources[x]]["vel"].append(spectral_signal_locations[i])
            signal_sources[sources[x]]["brightness"].append(brightness[i])
        except KeyError:
            signal_sources[sources[x]] = {"vel": [spectral_signal_locations[i]],
                                          "brightness": [brightness[i]]}

    tables = []
    with np.errstate(under="ignore"):
        signal = gaussian(channels, signal_width)
    # For every antenna...
    for i in range(number_of_antennas):
        # Generate Tsys values and bandpasses.
        xx_tsys = xx_tsys_all[i]
        yy_tsys = yy_tsys_all[i]
        xx = xx_all[i]
        yy = yy_all[i]

        # Add signals.
        with np.errstate(under="ignore"):
            for j in range(number_of_signals):
                rolled = np.roll(brightness[j] * signal,
                                 spectral_signal_locations[j] - channels // 2 + int(channels * signal_edge_avoid))
                xx[spatial_signal_locations[j], :] += rolled
                yy[spatial_signal_locations[j], :] += rolled

        sp_dict = {"source": sources,
                   "date": dates,
                   "ra": ra,
                   "dec": dec,
                   "vel_start": vel_start,
                   "vel_end": vel_end,
                   "vel_inc": vel_inc,
                   "xx_tsys": xx_tsys,
                   "yy_tsys": yy_tsys,
                   "temp": temp,
                   "pressure": pressure,
                   "humidity": humidity,
                   "jyperk": jyperk,
                   "elev": elev,
                   "xx": xx,
                   "yy": yy}

        t = Table(sp_dict,
                  names=sp_dict.keys(),
                  meta={"telescope": "ATCA",
                        "antenna": i + 1,
                        "num_channels": channels,
                        "rest_freq": 44.06941,
                        "signals": signal_sources})
        tables.append(t)
        if write:
            if path is None:
                t.write("generated.ant%s.hdf5" % (i + 1), path="raw", compression=True, overwrite=True)
            else:
                t.write(path + "/generated.ant%s.hdf5" % (i + 1), path="raw", compression=True, overwrite=True)
    return tables
