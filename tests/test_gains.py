#!/usr/bin/env python

import pytest
import numpy as np

import brewmaster.gains as g


def test_pvapsat():
    answer = 9950.554131297544

    # Using an int.
    result = g.pvapsat(300)
    assert np.isclose(result, answer, atol=1e-6)
    assert type(result) == float

    # Using a float.
    result = g.pvapsat(300.0)
    assert np.isclose(result, answer, atol=1e-6)
    assert type(result) == float

    # Using a numpy array.
    result = g.pvapsat(np.array([300]))
    assert np.isclose(result, answer, atol=1e-6)
    assert type(result) == np.ndarray

    # Using a low temperature.
    result = g.pvapsat(200)
    assert np.isclose(result, 0, atol=1e-6)
    assert type(result) == float

    result = g.pvapsat(np.array([200]))
    assert np.isclose(result, 0, atol=1e-6)
    assert type(result) == np.ndarray


def test_refdry():
    nr, ni = g.refdry(44069410000, np.asarray([[300]]), 1, 0)
    assert np.isclose(nr, 0.00258886, rtol=0.01)
    assert np.isclose(ni, 1.09520172e-12, rtol=0.01)


def test_refvap():
    nr, ni = g.refvap(44069410000, np.asarray([[300]]), 100, 50)
    assert np.isclose(nr, 2.20048886, rtol=0.01)
    assert np.isclose(ni, 6.6848481e-06, rtol=0.01)


def test_Obs_telescope():
    with pytest.raises(ValueError):
        g.Obs(44069, 25, 100, 1, 70, -50, "asdf")


def test_Obs_types():
    # Ints.
    o = g.Obs(44069, 25, 100, 1, 70, -50, "ATCA")
    assert type(o.temp) == np.ndarray
    assert type(o.pressure) == np.ndarray
    assert type(o.humidity) == np.ndarray
    assert type(o.elev) == np.ndarray
    assert type(o.dec) == np.ndarray
    assert type(o.temp[0]) == np.float64
    assert type(o.pressure[0]) == np.float64
    assert type(o.humidity[0]) == np.float64
    assert type(o.elev[0]) == np.float64
    assert type(o.dec[0]) == np.float64

    # Floats.
    o = g.Obs(44069.41, 25.0, 100.0, 0.5, 70.0, -49.5, "ATCA")
    assert type(o.temp) == np.ndarray
    assert type(o.pressure) == np.ndarray
    assert type(o.humidity) == np.ndarray
    assert type(o.elev) == np.ndarray
    assert type(o.dec) == np.ndarray
    assert type(o.temp[0]) == np.float64
    assert type(o.pressure[0]) == np.float64
    assert type(o.humidity[0]) == np.float64
    assert type(o.elev[0]) == np.float64
    assert type(o.dec[0]) == np.float64

    # Lists.
    o = g.Obs(44069.41, [25.0], [100.0], [0.5], [70.0], [-49.5], "ATCA")
    assert type(o.temp) == np.ndarray
    assert type(o.pressure) == np.ndarray
    assert type(o.humidity) == np.ndarray
    assert type(o.elev) == np.ndarray
    assert type(o.dec) == np.ndarray
    assert type(o.temp[0]) == np.float64
    assert type(o.pressure[0]) == np.float64
    assert type(o.humidity[0]) == np.float64
    assert type(o.elev[0]) == np.float64
    assert type(o.dec[0]) == np.float64

    # Numpy arrays.
    o = g.Obs(44069.41,
              np.array([25.0]),
              np.array([100.0]),
              np.array([0.5]),
              np.array([70.0]),
              np.array([-49.5]),
              "ATCA")
    assert type(o.temp) == np.ndarray
    assert type(o.pressure) == np.ndarray
    assert type(o.humidity) == np.ndarray
    assert type(o.elev) == np.ndarray
    assert type(o.dec) == np.ndarray
    assert type(o.temp[0]) == np.float64
    assert type(o.pressure[0]) == np.float64
    assert type(o.humidity[0]) == np.float64
    assert type(o.elev[0]) == np.float64
    assert type(o.dec[0]) == np.float64


def test_gains():
    # Ints.
    answer = 77.21411764
    o = g.Obs(44069, 25, 100, 1, 70, -50, "ATCA")
    o.determine_tsys()
    assert np.isclose(o.tsys, answer, atol=1e-6)

    # Floats.
    answer = 77.21252315
    o = g.Obs(44069.41, 25.0, 100.0, 0.5, 70.0, -50.0, "ATCA")
    o.determine_tsys()
    assert np.isclose(o.tsys, answer, atol=1e-6)

    # Numpy arrays.
    answer = 77.21252315
    o = g.Obs(44069.41,
              np.asarray([25.0]),
              np.asarray([100.0]),
              np.asarray([0.5]),
              np.asarray([70.0]),
              np.asarray([-50.0]),
              "ATCA")
    o.determine_tsys()
    assert np.isclose(o.tsys, answer, atol=1e-6)


if __name__ == "__main__":
    # Introspect and run all the functions starting with "test".
    for f in dir():
        if f.startswith("test"):
            print(f)
            exec(f+"()")
