#!/usr/bin/env python

import numpy as np

from scipy import signal

from sslf.sslf import Spectrum as SLF

from brewmaster.generate_dummy_data import generate_tables
from brewmaster.quotient import Calibrate
import brewmaster.merge as m


def test_average_spectra_ideal():
    # Make a bunch of spectra with a small Gaussian signal.
    np.random.seed(1000)

    spectra = []
    vel = np.arange(2048)
    num_spectra = 6
    for i in range(num_spectra):
        spectrum = np.random.normal(size=2048)
        with np.errstate(under="ignore"):
            spectrum += np.roll(0.75 * signal.gaussian(2048, 5), 100)

        if i < num_spectra - 1:
            tsys = 90
        else:
            tsys = 180
        o = m.Spectrum(vel=vel,
                       sp=spectrum,
                       filename="asdf",
                       tsys=tsys)
        o.create_spline(vel_min=np.min(vel),
                        vel_max=np.max(vel),
                        vel_inc=np.diff(vel)[0])
        spectra.append(o)

    # Run the `average_spectra` function.
    averaged, _ = m.average_spectra(spectra)

    # Verify that none of the input spectra find a line, and their SNRs are
    # about 1.
    for s in spectra:
        o = SLF(s.spectrum, vel)
        o.find_cwt_peaks(scales=range(1, 10), snr=6.5, wavelet=signal.ricker)
        assert o.channel_peaks == []
        assert np.isclose(np.std(s.spectrum), 1, rtol=0.03)

    # The averaged spectrum does find a line, and has a much smaller SNR.
    o = SLF(averaged, vel)
    o.find_cwt_peaks(scales=range(1, 10), snr=6.5, wavelet=signal.ricker)
    assert np.isclose(o.channel_peaks, [1123], atol=2)
    assert np.isclose(np.std(averaged),
                      1/np.sqrt(num_spectra-0.5),
                      rtol=0.02)


def test_average_spectra_non_aligned():
    # Make a bunch of spectra with a small Gaussian signal.
    np.random.seed(1000)

    spectra = []
    velocities = []
    vel = np.arange(2048)
    for i in range(6):
        for _ in range(2):
            spectrum = np.random.normal(size=2048)
            with np.errstate(under="ignore"):
                spectrum += np.roll(0.75 * signal.gaussian(2048, 5), 100 - 100*i)

            new_vel = vel + 100*i
            o = m.Spectrum(vel=new_vel,
                           sp=spectrum,
                           filename="asdf",
                           tsys=90)
            spectra.append(o)
            velocities.append(new_vel)

    # For each spectrum, make a spline.
    vel_min = np.min([np.min(x.vel) for x in spectra])
    vel_max = np.max([np.max(x.vel) for x in spectra])
    vel_inc = np.min([np.diff(x.vel)[0] for x in spectra])
    for s in spectra:
        s.create_spline(vel_min,
                        vel_max,
                        vel_inc)

    # Run the `average_spectra` function.
    averaged, vel_full = m.average_spectra(spectra)

    # Verify that none of the input spectra find a line, and their SNRs are
    # about 1.
    for s in spectra:
        o = SLF(s.spectrum, s.vel)
        o.find_cwt_peaks(scales=range(1, 10), snr=6.5, wavelet=signal.ricker)
        assert o.channel_peaks == []
        assert np.isclose(np.std(s.spectrum), 1, rtol=0.03)

    # The averaged spectrum does find lines, but these are at the edges because
    # the noise is non-uniform across the spectrum.
    o = SLF(averaged, vel_full)
    o.find_cwt_peaks(scales=range(1, 10), snr=6.5, wavelet=signal.ricker)
    assert o.channel_peaks == [2541, 2483]

    # Test the spectrum's standard deviation at various parts.
    with np.errstate(under="ignore"):
        assert np.isclose(np.std(averaged[:100]), np.std(averaged[-100:]), rtol=0.05)
        assert np.isclose(np.std(averaged[100:200]), np.std(averaged[-200:-100]), rtol=0.08)
        assert np.isclose(np.std(averaged[200:300]), np.std(averaged[-300:-200]), rtol=0.05)
        assert np.isclose(np.std(averaged[700:1300]), 1/np.sqrt(12), atol=0.015)
        assert np.isclose(np.std(averaged[700:1300]), 1/np.sqrt(12), rtol=0.04)

    # Test that the injected signal is found when removing the edges of the input spectrum.
    o = SLF(averaged[700:1300], vel_full)
    o.find_cwt_peaks(scales=range(1, 10), snr=6.5, wavelet=signal.ricker)
    assert o.channel_peaks == [424]


def test_flatten_from_averaged():
    # Make a bunch of spectra with a small Gaussian signal.
    np.random.seed(1000)

    spectra = []
    vel = np.arange(2048)
    num_spectra = 6
    for i in range(num_spectra):
        spectrum = np.random.normal(size=2048)
        # Add a random slope to the spectrum.
        spectrum += 2*np.sin(np.random.normal() * vel/100)
        with np.errstate(under="ignore"):
            spectrum += np.roll(0.75 * signal.gaussian(2048, 5), 100)

        if i < num_spectra - 1:
            tsys = 90
        else:
            tsys = 180
        o = m.Spectrum(vel=vel,
                       sp=spectrum,
                       filename="asdf",
                       tsys=tsys)
        o.create_spline(vel_min=np.min(vel),
                        vel_max=np.max(vel),
                        vel_inc=np.diff(vel)[0])
        spectra.append(o)

    # Average the spectra, run the line finder, then flatten the spectra.
    averaged, _ = m.average_spectra(spectra)
    o = SLF(averaged, vel)
    o.find_cwt_peaks(scales=range(1, 10), snr=6.5, wavelet=signal.ricker)
    flattened = m.flatten_from_averaged(o, spectra)

    # Flatten again, and identify the injected signal.
    averaged, _ = m.average_spectra(flattened)
    o = SLF(averaged, vel)
    o.find_cwt_peaks(scales=range(1, 10), snr=6.5, wavelet=signal.ricker)
    assert o.channel_peaks == [1124]


def test_poorly_behaved():
    """
    Create 12 spectra, then introduce ripples into two of them. Verify
    that these two get flagged.
    """

    np.random.seed(1000)
    spectra = {}
    for i in range(1, 13):
        spectra[i] = np.random.normal(size=2048)
    spectra[2] += np.sin(np.arange(2048)/50) + np.sin(np.arange(2048)/500)
    spectra[8] += np.sin(np.arange(2048)/70) + np.sin(np.arange(2048)/400)

    objects = []
    for i, s in spectra.items():
        objects.append(m.Spectrum(vel=np.arange(2048), sp=s, filename=str(i), tsys=90))

    poorly_behaved = m.detect_poorly_behaved_spectra(objects, snr=5, num_chunks=40, window_length=151)

    # Check that the good spectra are still there.
    for i in range(1, 13):
        if i == 2 or i == 8:
            continue
        assert str(i) in [o.filename for o in objects]
    assert "2" not in [o.filename for o in objects]
    assert "8" not in [o.filename for o in objects]

    # Check that the bad spectra have been identified.
    assert "2" in [o.filename for o in poorly_behaved]
    assert "8" in [o.filename for o in poorly_behaved]
    for i in range(1, 13):
        if i == 2 or i == 8:
            continue
        assert str(i) not in [o.filename for o in poorly_behaved]


if __name__ == "__main__":
    # Introspect and run all the functions starting with "test".
    for f in dir():
        if f.startswith("test"):
            print(f)
            exec(f+"()")

    # test_average_spectra_ideal()
    # test_average_spectra_non_aligned()
