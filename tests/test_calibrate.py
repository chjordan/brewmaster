#!/usr/bin/env python

import os
import copy
import pytest
import numpy as np

from brewmaster.generate_dummy_data import generate_tables
from brewmaster.quotient import Calibrate


TEST_DIR = '/'.join(os.path.realpath(__file__).split('/')[0:-1])
CHANNELS = 1024
BIRDIES = range(128, CHANNELS, 128)


def pytest_generate_tests():
    np.random.seed(1)
    generate_tables(rows=5,
                    columns=10,
                    path=TEST_DIR,
                    number_of_antennas=1,
                    channels=CHANNELS)


def extended(edge_ignore=10):
    return Calibrate(TEST_DIR + "/generated.ant1.hdf5",
                     extended=True,
                     birdies=BIRDIES,
                     edge_ignore=edge_ignore,
                     rename_references=False)


def non_extended(birdies=BIRDIES):
    return Calibrate(TEST_DIR + "/generated.ant1.hdf5",
                     extended=False,
                     birdies=birdies,
                     edge_ignore=None,
                     rename_references=True,
                     reference="test")


def test_init():
    extended()


def test_prune():
    o = extended()
    o.prune(prune_radius=5)
    # 51 = 5 rows * 10 columns + 1 reference
    assert len(o.table) == 51


def test_flag_rfi():
    o = extended()
    # Take this opportunity to test pruning with None.
    o.prune(prune_radius=None)
    # Record the data before RFI flagging.
    xx_data = copy.copy(o.table["xx"])
    yy_data = copy.copy(o.table["yy"])
    # Inject a single-channel spike into a spectrum.
    o.table["yy"][-3][CHANNELS//3] *= 2
    # Perform the flagging.
    o.flag_birdies()
    o.flag_rfi()
    # Verify that the "flagged" channel has a similar value
    # to its neighbours.
    assert np.all(np.isclose(o.table["yy"][-3][CHANNELS//3],
                             [o.table["yy"][-3][CHANNELS//3-2],
                              o.table["yy"][-3][CHANNELS//3-1],
                              o.table["yy"][-3][CHANNELS//3+1],
                              o.table["yy"][-3][CHANNELS//3+2]],
                             rtol=0.005))
    # Verify that not all XX spectra are the same, because birdie
    # channels have been flagged.
    with pytest.raises(AssertionError):
        assert np.all(xx_data == o.table["xx"])
    # Now replace the birdie channels so that everything is equal.
    for b in BIRDIES:
        xx_data[:, b] = 0
        yy_data[:, b] = 0
        o.table["xx"][:, b] = 0
        o.table["yy"][:, b] = 0
    # Verify that all XX spectra are the same.
    assert np.all(xx_data == o.table["xx"])
    # Verify that all YY spectra are the same,
    # except the one that had RFI injected.
    with pytest.raises(AssertionError):
        assert np.all(yy_data == o.table["yy"])
    o.table["yy"][-3][CHANNELS//3] = yy_data[-3][CHANNELS//3]
    assert np.all(yy_data == o.table["yy"])


def test_flag_rfi2():
    o = non_extended(birdies=[])
    # Record the data before RFI flagging.
    xx_data = copy.copy(o.table["xx"])
    yy_data = copy.copy(o.table["yy"])
    # Inject a single-channel spike into a spectrum.
    o.table["yy"][-5][CHANNELS//3*2] *= 2
    # Perform the flagging.
    o.flag_rfi()
    # Verify that the "flagged" channel has a similar value
    # to its neighbours.
    assert np.all(np.isclose(o.table["yy"][-5][CHANNELS//3*2],
                             [o.table["yy"][-5][CHANNELS//3*2-2],
                              o.table["yy"][-5][CHANNELS//3*2-1],
                              o.table["yy"][-5][CHANNELS//3*2+1],
                              o.table["yy"][-5][CHANNELS//3*2+2]],
                             rtol=0.005))
    # Verify that all spectra are the same.
    assert np.all(xx_data == o.table["xx"])
    # Verify that all YY spectra are the same,
    # except the one that had RFI injected.
    with pytest.raises(AssertionError):
        assert np.all(yy_data == o.table["yy"])
    o.table["yy"][-5][CHANNELS//3*2] = yy_data[-5][CHANNELS//3*2]
    assert np.all(yy_data == o.table["yy"])


def test_calibrate():
    o = extended()
    o.calibrate()


def test_average_references():
    o = non_extended()
    o.average_references()


def test_quotient():
    o = extended()
    # Copy the reference and a source.
    # Perform the quotient.
    o.quotient()
    # Verify that the quotient is correct.


def test_write():
    o = non_extended()
    o.write()


def test_edge_ignore():
    with pytest.raises(ValueError):
        extended(edge_ignore=CHANNELS//2)


if __name__ == "__main__":
    # Introspect and run all the functions starting with "test".
    for f in dir():
        if f.startswith("test"):
            print(f)
            exec(f+"()")
