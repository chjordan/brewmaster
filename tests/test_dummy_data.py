#!/usr/bin/env python

import os
import glob

import numpy as np
from astropy.table import Table

from brewmaster.generate_dummy_data import generate_tables
from brewmaster.quotient import Calibrate
from brewmaster.merge import Merge
from brewmaster.grid import Grid


TEST_DIR = '/'.join(os.path.realpath(__file__).split('/')[0:-1])


def test_dummy_1_data_generation():
    np.random.seed(4)
    generate_tables(path=TEST_DIR)


def test_dummy_2_calibrate():
    for f in glob.glob(TEST_DIR + "/generated.ant*.hdf5"):
        t = Calibrate(f,
                      extended=False,
                      rename_references=True)
        t.flag_rfi()
        t.flag_weak_rfi()
        t.calibrate()
        t.average_references(smoothing=True)
        t.quotient()
        t.write()


def test_dummy_3_merge():
    scales = list(range(40, 100, 5))
    t = Merge(glob.glob(TEST_DIR + "/generated.ant*.hdf5"))
    t.find_common_sources()
    t.merge_spectra(scales)
    t.write(output_filename=TEST_DIR + "/generated.merged.hdf5")


def test_dummy_4_grid():
    g = Grid(TEST_DIR + "/generated.merged.hdf5",
             projection="galactic",
             bunit='K')
    g.setup_fits_header()
    g.grid()
    g.write(output_filename=TEST_DIR + "/generated.fits")

    [os.remove(x) for x in glob.glob("{0}/generated.????".format(TEST_DIR))]


def test_dummy_object():
    np.random.seed(1000)
    generate_tables(path=TEST_DIR)

    tables = []
    for f in glob.glob(TEST_DIR + "/generated.ant*.hdf5"):
        tables.append(Table.read(f, path="raw"))

    quotients = []
    for table in tables:
        t = Calibrate(table,
                      extended=False,
                      rename_references=True)
        t.flag_rfi()
        t.flag_weak_rfi()
        t.calibrate()
        t.average_references(smoothing=True)
        t.quotient()
        quotients.append(t.table)

    scales = list(range(40, 100, 5))
    m = Merge(quotients)
    m.find_common_sources()
    m.merge_spectra(scales)

    g = Grid(m.table,
             projection="galactic",
             bunit='K')
    g.setup_fits_header()
    g.grid()
    g.write(output_filename=TEST_DIR + "/generated.fits")

    [os.remove(x) for x in glob.glob("{0}/generated.????".format(TEST_DIR))]


def test_dummy_blend():
    np.random.seed(1000)
    generate_tables(path=TEST_DIR)

    files = []
    for i, f in enumerate(glob.glob(TEST_DIR + "/generated.ant*.hdf5")):
        if i < 3:
            files.append(Table.read(f, path="raw"))
        else:
            files.append(f)

    quotients = []
    for i, f in enumerate(files):
        t = Calibrate(f,
                      extended=False,
                      rename_references=True)
        t.flag_rfi()
        t.flag_weak_rfi()
        t.calibrate()
        t.average_references(smoothing=True)
        t.quotient()
        if i < 3:
            quotients.append(t.table)
        else:
            t.write()
            quotients.append(t.output_filename)

    scales = list(range(40, 100, 5))
    m = Merge(quotients)
    m.find_common_sources()
    m.merge_spectra(scales)
    m.write(TEST_DIR + "/generated.merged.hdf5")

    g = Grid(m.output_filename,
             projection="galactic",
             bunit='K')
    g.setup_fits_header()
    g.grid()
    g.write(output_filename=TEST_DIR + "/generated.fits")

    [os.remove(x) for x in glob.glob("{0}/generated.????".format(TEST_DIR))]


if __name__ == "__main__":
    # Introspect and run all the functions starting with "test".
    for f in dir():
        if f.startswith("test"):
            print(f)
            exec(f+"()")
