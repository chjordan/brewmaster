#!/usr/bin/env python

import os
import copy
import glob
import pytest
import numpy as np
from astropy.table import Table

from brewmaster.generate_dummy_data import generate_tables
from brewmaster.quotient import Calibrate
from brewmaster.merge import Merge


TEST_DIR = '/'.join(os.path.realpath(__file__).split('/')[0:-1])
CHANNELS = 1024
BIRDIES = range(128, CHANNELS, 128)


def extended():
    np.random.seed(100)
    generate_tables(path=TEST_DIR, number_of_antennas=2, channels=CHANNELS)
    for f in [TEST_DIR + "/generated.ant1.hdf5", TEST_DIR + "/generated.ant2.hdf5"]:
        c = Calibrate(f,
                      extended=True,
                      birdies=BIRDIES,
                      edge_ignore=10,
                      rename_references=False)
        c.calibrate()
        c.average_references()
        c.quotient()
        c.write()


def non_extended():
    np.random.seed(100)
    generate_tables(path=TEST_DIR, number_of_antennas=2, channels=CHANNELS)
    for f in [TEST_DIR + "/generated.ant1.hdf5", TEST_DIR + "/generated.ant2.hdf5"]:
        c = Calibrate(f,
                      extended=False,
                      birdies=None,
                      edge_ignore=None,
                      rename_references=True,
                      reference="test")
        c.calibrate()
        c.average_references(smoothing=False)
        c.quotient()
        c.write()


def test_init():
    extended()
    Merge([TEST_DIR + "/generated.ant1.hdf5", TEST_DIR + "/generated.ant2.hdf5"])

    [os.remove(x) for x in glob.glob("{0}/generated.*.hdf5".format(TEST_DIR))]


def test_merge_spectra():
    # Generate the data, and create an instance of the Merge class.
    non_extended()
    m = Merge([TEST_DIR + "/generated.ant1.hdf5", TEST_DIR + "/generated.ant2.hdf5"])

    # Determine the variances of the last spectrum in the data.
    variances = []
    for f in [TEST_DIR + "/generated.ant1.hdf5", TEST_DIR + "/generated.ant2.hdf5"]:
        t = Table.read(f, path="quotiented")
        variances.append(np.var(t["xx"][-1]))
        variances.append(np.var(t["yy"][-1]))

    # Copy a row, then add it to each table.
    dups = []
    for t in m.tables.values():
        dup = copy.deepcopy(t[-5])
        t.add_row(dup)
        dups.append(dup)
    number_of_rows = len(t)

    # Verify that the duplicate gets detected.
    m.find_common_sources()
    assert len(m.sources) == number_of_rows - 1
    for s in m.sources:
        for c in m.sources[s]:
            for f in m.sources[s][c]:
                if s == dups[0]["source"]:
                    assert len(m.sources[s][c][f]) == 2
                else:
                    assert len(m.sources[s][c][f]) == 1

    # Use the merge method.
    m.merge_spectra(scales=[40])
    m.write(TEST_DIR + "/generated.merged.hdf5")
    t2 = Table.read(TEST_DIR + "/generated.merged.hdf5", path="data")
    merged_spectrum = t2["spectrum"][-1]

    # Compare the last spectrum's standard deviation against the merged.
    predicted_std = np.sqrt(np.sum(variances)) / len(variances)
    actual_std = np.std(merged_spectrum)
    assert (actual_std - predicted_std) / predicted_std < 0.15

    [os.remove(x) for x in glob.glob("{0}/generated.*.hdf5".format(TEST_DIR))]


# def test_continuum():
#     """
#     Just check that passing continuum data doesn't cause problems.
#     """

#     # Generate the data, and create an instance of the Merge class.
#     np.random.seed(100)
#     generate_tables(path=TEST_DIR, number_of_antennas=2, channels=33)
#     for f in [TEST_DIR + "/generated.ant1.hdf5", TEST_DIR + "/generated.ant2.hdf5"]:
#         c = Calibrate(f,
#                       extended=True,
#                       birdies=True,
#                       edge_ignore=0,
#                       rename_references=False)
#         c.calibrate()
#         c.average_references()
#         c.quotient()
#         c.write()
#     m = Merge([TEST_DIR + "/generated.ant1.hdf5", TEST_DIR + "/generated.ant2.hdf5"])
#     m.find_common_sources()
#     m.merge_spectra(scales=[1])
#     m.write(TEST_DIR + "/generated.merged.hdf5")

#     [os.remove(x) for x in glob.glob("{0}/generated.*.hdf5".format(TEST_DIR))]


def test_single_file():
    np.random.seed(10)
    generate_tables(path=TEST_DIR, number_of_antennas=1, channels=CHANNELS)
    c = Calibrate(TEST_DIR + "/generated.ant1.hdf5",
                  extended=True,
                  birdies=BIRDIES,
                  edge_ignore=10,
                  rename_references=False)
    c.calibrate()
    c.average_references()
    c.quotient()
    c.write()
    Merge(TEST_DIR + "/generated.ant1.hdf5")

    [os.remove(x) for x in glob.glob("{0}/generated.*.hdf5".format(TEST_DIR))]


def test_polarisations():
    extended()

    # xx_only and yy_only shouldn't be possible.
    with pytest.raises(ValueError):
        Merge(TEST_DIR + "/generated.ant1.hdf5", xx_only=True, yy_only=True)

    # xx_only
    x = Merge([TEST_DIR + "/generated.ant1.hdf5", TEST_DIR + "/generated.ant2.hdf5"], xx_only=True)
    x.find_common_sources()
    x.merge_spectra(scales=[1])
    x.write(TEST_DIR + "/generated.merged.hdf5")

    # yy_only
    y = Merge([TEST_DIR + "/generated.ant1.hdf5", TEST_DIR + "/generated.ant2.hdf5"], yy_only=True)
    y.find_common_sources()
    y.merge_spectra(scales=[1])
    y.write(TEST_DIR + "/generated.merged.hdf5")

    # Make sure the spectra aren't the same!
    for i, x in enumerate(x.output_spectra["spectrum"]):
        assert np.all(x != y.output_spectra["spectrum"][i])

    [os.remove(x) for x in glob.glob("{0}/generated.*.hdf5".format(TEST_DIR))]


def test_missing_metadata():
    non_extended()

    # Remove the metadata to test a little more code.
    for x in [TEST_DIR + "/generated.ant1.hdf5", TEST_DIR + "/generated.ant2.hdf5"]:
        t = Table.read(x, path="quotiented")
        t.meta = {}
        t.write(x, path="quotiented", compression=True, overwrite=True, append=True)
    Merge([TEST_DIR + "/generated.ant1.hdf5", TEST_DIR + "/generated.ant2.hdf5"])

    [os.remove(x) for x in glob.glob("{0}/generated.*.hdf5".format(TEST_DIR))]


# def test_wrong_hdf5_path():
#     np.random.seed(100)
#     generate_tables(path=TEST_DIR, number_of_antennas=2, channels=CHANNELS)
#     with pytest.raises(ValueError):
#         Merge([TEST_DIR + "/generated.ant1.hdf5", TEST_DIR + "/generated.ant2.hdf5"], path="asdf")

#     [os.remove(x) for x in glob.glob("{0}/generated.*.hdf5".format(TEST_DIR))]


if __name__ == "__main__":
    # Introspect and run all the functions starting with "test".
    for f in dir():
        if f.startswith("test"):
            print(f)
            exec(f+"()")
