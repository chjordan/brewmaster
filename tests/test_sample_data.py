#!/usr/bin/env python

from brewmaster.miriad2hdf5 import MiriadFile


def test_miriad_convert():
    # Test the miriad2hdf5 functionality.
    # TODO.
    return True


if __name__ == "__main__":
    # Introspect and run all the functions starting with "test".
    for f in dir():
        if f.startswith("test"):
            print(f)
            exec(f+"()")
