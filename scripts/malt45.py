#!/usr/bin/env python

# This script is intended for MALT-45 data. It should be run from a directory
# containing a folder called "RAW", which contains the raw RPFITS files from
# the ATCA.

import os
import re
import sys
import glob
import shutil
import argparse
import subprocess as s
import multiprocessing as mp

import numpy as np

from brewmaster.quotient import Calibrate
from brewmaster.merge import Merge
from brewmaster.grid import Grid


# Import tqdm without enforcing it as a dependency
try:
    from tqdm import tqdm
except ImportError:
    def tqdm(*args, **kwargs):
        if args:
            return args[0]
        return kwargs.get('iterable', None)


# The following dictionary has keys for rest frequencies, each pointing to a
# molecular transition, and if it's extended emission.
ifs = {
    "42.5193826": ["SiO_v3", False],
    "42.8205864": ["SiO_v2", False],
    "42.95197":   ["H53a", True],
    "43.1220747": ["SiO_v1", False],
    "43.4238530": ["SiO_v0", True],
    "44.06941":   ["CH3OH", False],
    "48.15360":   ["H51a", True],
    "48.3724558": ["CH3OH_thermal", True],
    "48.6516043": ["OCS", True],
    "48.9909549": ["CS", True],
    "48.2069411": ["C34S", True],
    "43.195":     ["continuum_1", False],
    "48.610":     ["continuum_2", False],
    "42.203":     ["dummy1", True],
    "47.618":     ["dummy2", True],
}

ignore_sources = "-source(point),-source(1646-50),-source(1253-055),-source(1934-638),-source(1921-293),-source(mars),-source(1322-42)"


parser = argparse.ArgumentParser()
parser.add_argument("-v", "--verbosity", action="store_false",
                    help="Disable progress bars.")
parser.add_argument("-r", "--reference", default="ref",
                    help="Specify source to use as a reference for quotienting.")
parser.add_argument("-p", "--prune", default=5,
                    help="Prune sources outside this many degrees from the mean Galactic longitude.")
parser.add_argument("-c", "--skip_calibration", action="store_true",
                    help="If enabled, do not do the calibration/quotient step (useful if you're testing other things).")
parser.add_argument("-m", "--skip_merging", action="store_true",
                    help="If enabled, do not do the merging step (useful if you're testing other things).")
parser.add_argument("-g", "--skip_gridding", action="store_true",
                    help="If enabled, do not do the gridding/imaging step (useful if you're testing other things).")
parser.add_argument("files", nargs='*',
                    help="Files to be processed.")
args = parser.parse_args()


# Exit if there is no RAW directory available.
if not os.path.exists("RAW"):
    print("No RAW directory here! Exiting.")
    exit(1)


# Make a folder PROCESSING if it does not already exist.
if not os.path.exists("PROCESSING"):
    os.mkdir("PROCESSING")
os.chdir("PROCESSING")

# If there are already hdf5 files in PROCESSING, then we don't need to run
# atlod again.
if not any(glob.glob("*.hdf5")):
    # Import here, so that we could run other parts of this script without
    # requiring aipy to be installed.
    from brewmaster.miriad2hdf5 import MiriadFile

    s.call("atlod in=../RAW/* out=atlod options=nocross,notsys,birdie,opcorr".split())
    s.call("uvsplit vis=atlod options=nosource,ifchain".split())

    # The following (convoluted, inefficient) code determines which uvsplit file
    #  corresponds to a spectral line.
    rest_freqs = np.array(list(ifs.keys()))
    rest_freqs_floats = np.array(rest_freqs, dtype=float)
    corr_freqs = {}
    for i, f in enumerate(glob.glob("uvsplit*")):
        corr_freqs[f] = re.match(r"uvsplit\.\d\.(\d+)$", f).group(1)
    proper = {}
    while len(rest_freqs) > 0:
        minimum = (None, 0, "")
        for (filename, corr_freq) in corr_freqs.items():
            diff = np.abs(rest_freqs_floats - (float(corr_freq) / 1000))
            i = diff.argmin()
            m = diff[i]
            if minimum[0] is None or m < minimum[0]:
                minimum = (m, rest_freqs[i], filename, i)
        proper[minimum[2]] = minimum[1]
        i = minimum[3]
        rest_freqs = np.delete(rest_freqs, i)
        rest_freqs_floats = np.delete(rest_freqs_floats, i)

    for (filename, freq) in proper.items():
        # Set the appropriate rest frequency to the MIRIAD file.
        s.call(("puthd in=%s/restfreq value=%s type=double" % (filename, freq)).split())

        # Split the IF into per-antenna files.
        mol_name = ifs[freq][0]
        if "dummy" in mol_name:
            continue
        for a in range(1, 7):
            s.call((("uvcat vis=%s stokes=xx,yy select=%s,ant(%s)(%s) out=%s.ant%s") %
                   (filename, ignore_sources, a, a, mol_name, a)).split())

    # Convert the newly split per-antenna files into a hdf5 format, then delete
    # the MIRIAD file.
    def convert(filename):
        m = MiriadFile(filename)
        m.unpack()
        m.write()
        shutil.rmtree(filename, ignore_errors=True)
    pool = mp.Pool()
    pool.map(convert, glob.glob("*ant?"))
    pool.close()

    # Remove "atlod" and uvsplit files to conserve space.
    shutil.rmtree("atlod", ignore_errors=True)
    for f in glob.glob("uvsplit*"):
        shutil.rmtree(f, ignore_errors=True)


# Calibration.
# Any spectra with RFI are flagged, Tsys values are corrected, and bandpass
# gains are made elevation-independent before performing a quotient for each
# source spectrum. "Pruning" can also remove sources that are too far away
# from the median Galactic longitude; using a prune_radius of 0 disables
# pruning.

# This function demonstrates usage of the "Calibrate" class.
def calibrate(pair):
    filename, extended = pair
    t = Calibrate(filename,
                  extended=extended,
                  rename_references=True,
                  reference=args.reference,
                  path="raw")
    t.prune(prune_radius=args.prune)  # Defined by the command-line arguments.
    t.flag_birdies()
    t.flag_rfi()
    # At this point, it is possible that all data have been flagged. Check that
    # data is still present.
    if t.table["xx"].shape[0] == 0 and t.table["yy"].shape[0] == 0:
        print("WARNING: File {} was completely flagged by flag_rfi.".format(filename),
              file=sys.stderr)
        return
    t.flag_weak_rfi()
    t.calibrate()
    t.average_references(smoothing=True)
    t.quotient()
    t.write(path="quotiented")


# The following block iterates through all IFs, and calibrates each antenna
# in parallel.
if not args.skip_calibration:
    files = []
    for (mol_name, extended) in sorted(ifs.values()):
        # Skip continuum for now; it's a WIP.
        if "continuum" in mol_name:
            continue
        if "dummy" in mol_name:
            continue
        for f in sorted(glob.glob("%s.ant?.hdf5" % mol_name)):
            files.append((f, extended))
    pool = mp.Pool()
    list(tqdm(pool.imap(calibrate, files),
              total=len(files),
              desc="Calibrating",
              disable=not args.verbosity))
    pool.close()


# Merging.
# The spectra measured by each antenna at each source position is averaged, then
# searched for lines before performing a baseline subtraction. The result is a
# single file with a single spectrum for each source, which is ready to be
# gridded.

# This function demonstrates usage of the "Merge" class.
def merge(files, path="data"):
    mol_name = files[0].split('.')[0]
    t = Merge(files, path="quotiented")
    t.find_common_sources()
    t.merge_spectra(scales=list(range(4, 20, 2)) +
                           list(range(20, 40, 3)) +
                           list(range(40, 100, 5)))
    t.write(output_filename="%s.merged.hdf5" % mol_name,
            path=path)


# The following block iterates through all IFs, and merges all antennas
# in parallel.
if not args.skip_merging:
    all_files = []
    for (mol_name, _) in sorted(ifs.values()):
        # Skip continuum for now; it's a WIP.
        if "continuum" in mol_name:
            continue
        if "dummy" in mol_name:
            continue
        mol_files = sorted(glob.glob("%s.ant?.hdf5" % mol_name))
        if mol_files:
            all_files.append(mol_files)
    for a in tqdm(all_files,
                  desc="Merging",
                  disable=not args.verbosity):
        merge(a)


# Imaging.
def image(hdf5_file, path="data", output_filename=None):
    if output_filename is None:
        basename, _ = os.path.splitext(hdf5_file)
        basename = basename.split('/')[-1]
        output_filename = "%s.fits" % basename
    g = Grid(hdf5_file, path=path)
    g.setup_fits_header()
    g.grid(kernel_type="gauss1d",
           kernel_size_sigma=0.4,
           kernel_support=3,
           hpx_maxres=0.5)
    g.mom_minus2()
    g.write(output_filename=output_filename)


# The merged products are gridded to produce cubes.
os.chdir("..")
if not args.skip_gridding:
    for (mol_name, _) in tqdm(ifs.values(), desc="Gridding", disable=not args.verbosity):
        # Skip continuum for now; it's a WIP.
        if "continuum" in mol_name or "dummy" in mol_name:
            continue
        filename = glob.glob("PROCESSING/%s.merged.hdf5" % mol_name)
        if filename:
            image(filename[0])
