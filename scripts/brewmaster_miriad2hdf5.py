#!/usr/bin/env python

import os
import sys
import logging
import argparse
import multiprocessing as mp

from brewmaster.miriad2hdf5 import MiriadFile


def miriad2hdf5(filename):
    m = MiriadFile(filename)

    if not args.overwrite and os.path.exists(m.hdf5_filename):
        print("File %s exists; exiting." % m.hdf5_filename, file=sys.stderr)
    else:
        m.unpack()
        m.write()


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verbosity", action="count", default=0,
                        help="Display messages.")
    parser.add_argument("-o", "--overwrite", action="store_true",
                        help="Overwrite existing files.")
    parser.add_argument("files", nargs='*', help="Files to be processed.")
    args = parser.parse_args()

    if args.verbosity == 1:
        logging.basicConfig(level=logging.INFO)
    elif args.verbosity > 1:
        logging.basicConfig(level=logging.DEBUG)

    if len(args.files) > 1 and args.verbosity == 0:
        mp.Pool().map(miriad2hdf5, args.files)
    else:
        for f in args.files:
            print("Converting %s" % f)
            miriad2hdf5(f)
