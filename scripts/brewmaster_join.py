#!/usr/bin/env python

import os
import argparse

import numpy as np
from scipy.interpolate import InterpolatedUnivariateSpline as spline
from astropy.table import Table

# Import tqdm without enforcing it as a dependency
try:
    from tqdm import tqdm
except ImportError:
    def tqdm(*args, **kwargs):
        if args:
            return args[0]
        return kwargs.get('iterable', None)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verbosity", action="store_true",
                        help="Display messages.")
    parser.add_argument("-o", "--output_filename", required=True,
                        help="Filename of joined HDF5 file.")
    parser.add_argument("--min", type=float,
                        help="Specify the minimum velocity cutoff.")
    parser.add_argument("--max", type=float,
                        help="Specify the maximum velocity cutoff.")
    parser.add_argument("--inc", type=float,
                        help="Specify the velocity increment.")
    parser.add_argument("--hdf5_path", default="data",
                        help="Use this path of the HDF5 files for reading and writing. Default: %(default)s.")
    parser.add_argument("files", nargs='*', help="Files to be processed.")
    args = parser.parse_args()

    # Sanity checks.
    if not args.files:
        print("No files supplied! Exiting.")
        exit(1)

    if args.output_filename is None:
        raise ValueError("Output filename is required when joining (specify with -o).")

    # Check if the output has an extension or not.
    _, ext = os.path.splitext(args.output_filename)
    if ext.lower() != ".hdf5":
        raise ValueError("Output filename extension is not hdf5!")
        exit(1)

    output = {
        "source": [],
        "ra": [],
        "dec": [],
        "glon": [],
        "glat": [],
    }
    splines = []

    # The smallest and largest velocities of all data needs to be determined,
    # such that all data are re-gridded to a new "global" velocity axis.
    vel_min = None
    vel_max = None
    vel_inc = None
    for f in args.files:
        t = Table.read(f, path=args.hdf5_path)
        vmin = t.meta["vel_min"]
        vmax = t.meta["vel_max"]
        vinc = t.meta["vel_inc"]
        vel = np.arange(vmin, vmax, vinc)

        if vel_min is None or vmin < vel_min:
            vel_min = vmin
        if vel_max is None or vmax > vel_max:
            vel_max = vmax
        if vel_inc is None or vinc < vel_inc:
            vel_inc = vinc

        for i, _ in enumerate(tqdm(t,
                                   desc="Unpacking data from %s" % f,
                                   disable=not args.verbosity)):
            spectrum = t["spectrum"][i]
            nan_indicies = np.isnan(spectrum)
            spectrum[nan_indicies] = 0

            spl = spline(vel, spectrum, ext="zeros")
            splines.append(spl)
            for key in ["ra", "dec", "glon", "glat", "source"]:
                if isinstance(t[key][i], str):
                    output[key].append(t[key][i].encode("UTF-8"))
                else:
                    output[key].append(t[key][i])

    if args.min is not None:
        vel_min = args.min
    if args.max is not None:
        vel_max = args.max
    if args.inc is not None:
        vel_max = args.inc

    # Assume all tables have the same properties.
    rest_freq = t.meta["rest_freq"]
    extended = t.meta["extended"]
    telescope = t.meta["telescope"]
    continuum = t.meta["continuum"]

    vel_full = np.arange(vel_min, vel_max, vel_inc)
    full_splines = np.empty([len(splines), len(vel_full)], dtype=np.float32)
    masks = np.empty([len(splines), len(vel_full)], dtype=int)
    for i, s in enumerate(splines):
        full_spline = s(vel_full)
        zero_indices = np.where(full_spline == 0)[0]
        mask = np.ones_like(full_spline)
        mask[zero_indices] = 0
        full_splines[i, :] = full_spline
        masks[i, :] = mask

    output["spectrum"] = full_splines
    output["mask"] = masks

    t = Table(output,
              names=("source",
                     "ra",
                     "dec",
                     "glon",
                     "glat",
                     "spectrum",
                     "mask"),
              meta={"vel_min": vel_min,
                    "vel_max": vel_max,
                    "vel_inc": vel_inc,
                    "rest_freq": rest_freq,
                    "telescope": telescope,
                    "extended": extended,
                    "continuum": continuum,
                    "derived_from": ','.join(args.files)})
    t.write(args.output_filename, path=args.hdf5_path, compression=True, overwrite=True, append=True)
    if args.verbosity:
        print("Written: %s" % args.output_filename)
