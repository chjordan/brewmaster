#!/usr/bin/env python

import logging
import argparse

from brewmaster.grid import Grid


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--output_filename",
                        help="Output filename of the gridded cube.")
    parser.add_argument("--mom-2", action="store_true",
                        help="Produce a peak intensity (a.k.a. moment -2) map.")
    parser.add_argument("--mom0", action="store_true",
                        help="Produce a integrated intensity (a.k.a. moment 0) map.")
    parser.add_argument("-v", "--verbosity", action="store_true",
                        help="Display messages.")
    parser.add_argument("-p", "--projection", default="galactic",
                        help="Choose between equatorial or galactic coordinate systems for the "
                        "output cube; use j2000 or galactic. Default: %(default)s")
    parser.add_argument("-b", "--bunit", default=None,
                        help="Specify the BUNIT (typically 'K' or 'Jy').")
    parser.add_argument("-f", "--freq", type=float, default=None,
                        help="Specify the rest frequency of the spectral line.")
    parser.add_argument("-c", "--calibration_factor", type=float,
                        help="Specify the flux calibration factor (antenna efficiency and K-to-Jy factor).")
    parser.add_argument("--hdf5_path", default="data",
                        help="Use this path of the HDF5 files for gridding. Default: %(default)s.")
    parser.add_argument("--no_overwrite", action="store_true",
                        help="Don't overwrite files if they already exist.")
    parser.add_argument("files", nargs='*', help="Files to be processed.")
    args = parser.parse_args()

    if args.verbosity == 1:
        logging.basicConfig(level=logging.INFO)
    elif args.verbosity > 1:
        logging.basicConfig(level=logging.DEBUG)

    # Strategy when handling input files: if there's more than one, then we
    # cannot use the --output_filename option, so we let the Grid class name the
    # file for us.
    if len(args.files) > 1 and args.output_filename:
        logging.error("Cannot use --output_filename when specifying multiple files. "
                      "Either run this script over each file individually, or let this script "
                      "decide a new filename for each input file by omitting --output_filename.")
        exit(1)

    for f in args.files:
        logging.info("Gridding: %s", f)
        g = Grid(f,
                 rest_freq=args.freq,
                 projection=args.projection,
                 bunit=args.bunit,
                 path=args.hdf5_path,
                 verbosity=args.verbosity)
        g.setup_fits_header()
        g.grid()
        if args.mom_2:
            g.mom_minus2()
        if args.mom0:
            g.mom_0()

        if len(args.files) == 1:
            g.write(output_filename=args.output_filename, overwrite=not args.no_overwrite)
        else:
            g.write(overwrite=not args.no_overwrite)
