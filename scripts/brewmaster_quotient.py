#!/usr/bin/env python

import sys
import logging
import argparse
import multiprocessing as mp

from brewmaster.quotient import Calibrate


def raw2quotiented(hdf5_filename):
    t = Calibrate(hdf5_filename,
                  reference=args.reference,
                  extended=extended,
                  path=args.read_hdf5_path)
    t.prune(prune_radius=args.prune)
    t.flag_birdies()
    t.flag_rfi()
    # At this point, it is possible that all data have been flagged. Check that
    # data is still present.
    if t.table["xx"].shape[0] == 0 and t.table["yy"].shape[0] == 0:
        print("WARNING: File {} was completely flagged by flag_rfi.".format(hdf5_filename),
              file=sys.stderr)
        return
    t.flag_weak_rfi()
    t.calibrate()
    t.average_references(smoothing=True, smooth_window=args.smooth_window)
    t.quotient(debugging=args.debugging)
    t.write(path=args.write_hdf5_path)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verbosity", action="count", default=0,
                        help="Display status messages. Disables parallel processing of files.")
    parser.add_argument("-d", "--debugging", action="store_true",
                        help="Display more messages.")
    parser.add_argument("-+e", "--extended", action="store_true", default=None,
                        help="If set, indicates that the emission in this data is extended.")
    parser.add_argument("-e", "--not_extended", action="store_true", default=None,
                        help="If set, indicates that the emission in this data is NOT extended.")
    parser.add_argument("-r", "--reference", default="ref",
                        help="Specify source to use as a reference for quotienting.")
    parser.add_argument("-p", "--prune", default=0, type=float,
                        help="Prune sources outside this many degrees from the mean Galactic longitude.")
    parser.add_argument("-s", "--smooth_window", type=int, default=None,
                        help="Use this this many channels when smoothing reference spectra.")
    parser.add_argument("--read_hdf5_path", default="raw",
                        help="Use this path of the HDF5 files for quotienting. Default: %(default)s.")
    parser.add_argument("--write_hdf5_path", default="quotiented",
                        help="Write to this path of the output HDF5 file. Default: %(default)s.")
    parser.add_argument("files", nargs='*',
                        help="Files to be processed.")
    args = parser.parse_args()

    if args.verbosity == 1:
        logging.basicConfig(level=logging.INFO)
    elif args.verbosity > 1:
        logging.basicConfig(level=logging.DEBUG)

    # Assume that the emission is extended, but adjust from command-line
    # arguments.
    extended = True
    if args.extended and args.not_extended:
        print("extended and not_extended cannot both be set simultaneously.", file=sys.stderr)
        exit(1)
    elif args.not_extended:
        extended = False

    if args.verbosity > 0 or len(args.files) == 1:
        logger = logging.getLogger(__file__)
        for f in args.files:
            logger.info("Quotienting: %s" % f)
            raw2quotiented(f)
    else:
        mp.Pool().map(raw2quotiented, args.files)
