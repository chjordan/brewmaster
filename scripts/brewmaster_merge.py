#!/usr/bin/env python

import os
import logging
import argparse
import multiprocessing as mp

from brewmaster.merge import Merge
from scipy.signal import ricker, gaussian
from brewmaster.wavelets import tukey, wide_and_flat_topped


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-v", "--verbosity", action="count", default=0,
                        help="Display status messages.")
    parser.add_argument("-d", "--debugging", action="count", default=0,
                        help="Show plots within routines.")
    parser.add_argument("-x", "--xx_only", action="store_true",
                        help="Use only XX polarisation data.")
    parser.add_argument("-y", "--yy_only", action="store_true",
                        help="Use only YY polarisation data.")
    parser.add_argument("-o", "--output_filename", required=True,
                        help="Path to the HDF5 file to write out.")
    parser.add_argument("-s", "--filter_all_sources_except",
                        help="Use only this source when processing; useful for debugging.")
    parser.add_argument("-b", "--blank_additional_channels", default=(3, 15),
                        help="Blank this many additional channels around each detected spectral line according to t[0]*np.sqrt(x) + t[1], where x is a spectral line SNR. Default: %(default)s")
    parser.add_argument("-p", "--poly_order", default=1, type=int,
                        help="The order of the polynomial to be removed from spectra before merging them. Default: %(default)s.")
    parser.add_argument("-w", "--wavelet", default="ricker",
                        help="The wavelet to use when finding spectral lines. Default: %(default)s.")
    parser.add_argument("--processes", default=mp.cpu_count(), type=int,
                        help="The number of processes to use for parallel code. Default: %(default)s.")
    parser.add_argument("--read_hdf5_path", default="quotiented",
                        help="Use this path of the HDF5 files for merging. Default: %(default)s.")
    parser.add_argument("--write_hdf5_path", default="data",
                        help="Write to this path of the output HDF5 file. Default: %(default)s.")
    parser.add_argument("hdf5_files", nargs='*', help="Files to be processed.")
    args = parser.parse_args()

    if args.verbosity == 1:
        logging.basicConfig(level=logging.INFO)
    elif args.verbosity > 1:
        logging.basicConfig(level=logging.DEBUG)

    # Check if the output has an extension or not.
    _, ext = os.path.splitext(args.output_filename)
    if ext.lower() != ".hdf5":
        logging.error("Output filename extension is not hdf5! Exiting.")
        exit(1)

    scales = list(range(4, 20, 2)) + list(range(20, 40, 3)) + list(range(40, 100, 5))

    if args.wavelet.lower() == "ricker":
        wavelet = ricker
    elif args.wavelet.lower() == "gaussian":
        wavelet = gaussian
    elif args.wavelet.lower() == "tukey":
        wavelet = tukey
    elif "wide" in args.wavelet.lower() or "flat" in args.wavelet.lower():
        wavelet = wide_and_flat_topped

    if isinstance(args.blank_additional_channels, tuple):
        pass
    else:
        try:
            fst, snd = args.blank_additional_channels.split(",")
            args.blank_additional_channels = (float(fst), float(snd))
        except ValueError:
            logging.error("Couldn't parse --blank_additional_channels. Specify like: \"3, 15\"")
            exit(1)
    logging.debug("--blank_additional_channels = %s", args.blank_additional_channels)

    t = Merge(args.hdf5_files,
              debugging=args.debugging,
              xx_only=args.xx_only,
              yy_only=args.yy_only,
              path=args.read_hdf5_path)
    t.find_common_sources(filter_all_sources_except=args.filter_all_sources_except)
    t.merge_spectra(scales,
                    poly_order=args.poly_order,
                    blank_additional_channels=args.blank_additional_channels,
                    wavelet=wavelet,
                    processes=args.processes)
    t.write(output_filename=args.output_filename, path=args.write_hdf5_path)
